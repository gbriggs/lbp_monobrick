# LBP MonoRobot #

A simple application to control your Lego EV3 robot from your Raspberry Pi or Windows PC.

![homeTabConnected.png](https://bitbucket.org/repo/xr4red/images/4255578965-homeTabConnected.png)

The MonoRobot assembly is built using the open source [MonoBrick](http://www.monobrick.dk) assembly.  MonoRobot wraps up the interface between the program and the robot control functions available in MonoBrick. 

The ev3 Control Panel (ev3CPanel) application is the user interface layer for the MonoRobot control functions. The ev3CPanel is a simple Windows Forms application that runs on Linux (with Mono) and Windows PC.

Joystick control is implemented in the SimpleJoy assembly. This assembly supports Xbox controllers on Linux ([using this code](http://mpolaczyk.pl/raspberry-pi-mono-c-joystick-handler/)), and Wiimote controllers on Windows ([using this code](http://wiimotelib.codeplex.com)).

For more informations, please see our website at:  [LittleBytesOfPi.com/ev3](http://littlebytesofpi.com/ev3)