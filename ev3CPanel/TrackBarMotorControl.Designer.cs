﻿namespace ev3CPanel
{
    partial class TrackBarMotorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxMotor = new System.Windows.Forms.GroupBox();
            this.trackBarMotor = new System.Windows.Forms.TrackBar();
            this.labelMotorSpeed = new System.Windows.Forms.Label();
            this.buttonMotorAStop = new System.Windows.Forms.Button();
            this.labelMotorTach = new System.Windows.Forms.Label();
            this.groupBoxMotor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMotor)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxMotor
            // 
            this.groupBoxMotor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMotor.BackColor = System.Drawing.SystemColors.Control;
            this.groupBoxMotor.Controls.Add(this.trackBarMotor);
            this.groupBoxMotor.Controls.Add(this.labelMotorSpeed);
            this.groupBoxMotor.Controls.Add(this.buttonMotorAStop);
            this.groupBoxMotor.Controls.Add(this.labelMotorTach);
            this.groupBoxMotor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxMotor.Location = new System.Drawing.Point(4, 3);
            this.groupBoxMotor.Name = "groupBoxMotor";
            this.groupBoxMotor.Size = new System.Drawing.Size(110, 319);
            this.groupBoxMotor.TabIndex = 23;
            this.groupBoxMotor.TabStop = false;
            this.groupBoxMotor.Text = "Motor _";
            // 
            // trackBarMotor
            // 
            this.trackBarMotor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarMotor.Location = new System.Drawing.Point(30, 20);
            this.trackBarMotor.Maximum = 20;
            this.trackBarMotor.Minimum = -20;
            this.trackBarMotor.Name = "trackBarMotor";
            this.trackBarMotor.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarMotor.Size = new System.Drawing.Size(45, 218);
            this.trackBarMotor.TabIndex = 9;
            this.trackBarMotor.Scroll += new System.EventHandler(this.trackBarMotor_Scroll);
            // 
            // labelMotorSpeed
            // 
            this.labelMotorSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMotorSpeed.AutoSize = true;
            this.labelMotorSpeed.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotorSpeed.Location = new System.Drawing.Point(6, 239);
            this.labelMotorSpeed.Name = "labelMotorSpeed";
            this.labelMotorSpeed.Size = new System.Drawing.Size(64, 16);
            this.labelMotorSpeed.TabIndex = 8;
            this.labelMotorSpeed.Text = "_ Speed";
            // 
            // buttonMotorAStop
            // 
            this.buttonMotorAStop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMotorAStop.Location = new System.Drawing.Point(19, 286);
            this.buttonMotorAStop.Name = "buttonMotorAStop";
            this.buttonMotorAStop.Size = new System.Drawing.Size(67, 27);
            this.buttonMotorAStop.TabIndex = 7;
            this.buttonMotorAStop.Text = "Stop";
            this.buttonMotorAStop.UseVisualStyleBackColor = true;
            this.buttonMotorAStop.Click += new System.EventHandler(this.buttonMotorStop_Click);
            // 
            // labelMotorTach
            // 
            this.labelMotorTach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMotorTach.AutoSize = true;
            this.labelMotorTach.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotorTach.Location = new System.Drawing.Point(6, 261);
            this.labelMotorTach.Name = "labelMotorTach";
            this.labelMotorTach.Size = new System.Drawing.Size(56, 16);
            this.labelMotorTach.TabIndex = 6;
            this.labelMotorTach.Text = "_ Tach";
            // 
            // TrackBarMotorControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.groupBoxMotor);
            this.Name = "TrackBarMotorControl";
            this.Size = new System.Drawing.Size(117, 325);
            this.groupBoxMotor.ResumeLayout(false);
            this.groupBoxMotor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMotor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxMotor;
        private System.Windows.Forms.Label labelMotorSpeed;
        private System.Windows.Forms.Button buttonMotorAStop;
        private System.Windows.Forms.Label labelMotorTach;
        private System.Windows.Forms.TrackBar trackBarMotor;
      
    }
}
