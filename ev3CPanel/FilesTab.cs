﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MonoRobot;
using MonoBrick.EV3;


namespace ev3CPanel
{
    public partial class FilesTab : UserControl
    {
        Robot Robot { get; set; }
        public FilesTab()
        {
            InitializeComponent();

        }

        public void InitFileTabPage(Robot robot)
        {
            Robot = robot;
            UpdateBrickFiles();
        }

        public ListView ListViewFiles { get { return listViewFiles; } }

        public void UpdateBrickFiles()
        {
            List<BrickFile> files = Robot.BrickFiles;

            listViewFiles.Items.Clear();

            foreach (var nextFile in files)
            {
                ListViewItem nextItem = new ListViewItem(nextFile.FullName.TrimStart("/home/root/lms2012/prjs".ToCharArray()));
                nextItem.Tag = nextFile;
                listViewFiles.Items.Add(nextItem);
            }
        }

        private void buttonStartProgram_Click(object sender, EventArgs e)
        {
            if (listViewFiles.SelectedItems != null && listViewFiles.SelectedItems.Count > 0)
            {
                Robot.RunProgram((BrickFile)listViewFiles.SelectedItems[0].Tag);
            }
            else
            {
                MessageBox.Show("Please select a program to run.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void buttonStopProgram_Click(object sender, EventArgs e)
        {
            Robot.StopRunningProgram();
        }
    }
}
