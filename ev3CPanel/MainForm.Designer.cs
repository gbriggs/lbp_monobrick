﻿namespace ev3CPanel
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConnect = new System.Windows.Forms.Button();
            this.labelConnection = new System.Windows.Forms.Label();
            this.backgroundWorkerConnection = new System.ComponentModel.BackgroundWorker();
            this.tabControlRobot = new System.Windows.Forms.TabControl();
            this.tabPageRobot = new System.Windows.Forms.TabPage();
            this.groupBoxLightSwitch = new System.Windows.Forms.GroupBox();
            this.checkBoxLightSwitchWelder = new System.Windows.Forms.CheckBox();
            this.checkBoxLightSwitchCore = new System.Windows.Forms.CheckBox();
            this.checkBoxLightSwitchPwrMain = new System.Windows.Forms.CheckBox();
            this.checkBoxLightSwitchPwr2 = new System.Windows.Forms.CheckBox();
            this.checkBoxLightSwitchPwr1 = new System.Windows.Forms.CheckBox();
            this.checkBoxLightSwitchCtrlTwr = new System.Windows.Forms.CheckBox();
            this.groupBoxSensors = new System.Windows.Forms.GroupBox();
            this.checkBoxTouchLight = new System.Windows.Forms.CheckBox();
            this.checkBoxIrLights = new System.Windows.Forms.CheckBox();
            this.checkBoxColorLights = new System.Windows.Forms.CheckBox();
            this.checkBoxMonitorTouch = new System.Windows.Forms.CheckBox();
            this.checkBoxMonitorIr = new System.Windows.Forms.CheckBox();
            this.labelSensorStatus = new System.Windows.Forms.Label();
            this.checkBoxMonitorColorSensor = new System.Windows.Forms.CheckBox();
            this.groupBoxMotors = new System.Windows.Forms.GroupBox();
            this.checkBoxMonitorMotorsTach = new System.Windows.Forms.CheckBox();
            this.labelMotorStatus = new System.Windows.Forms.Label();
            this.checkBoxMonitorMotorsSpeed = new System.Windows.Forms.CheckBox();
            this.groupBoxJoystick = new System.Windows.Forms.GroupBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonConnectJoystick = new System.Windows.Forms.Button();
            this.comboBoxJoystickPaths = new System.Windows.Forms.ComboBox();
            this.tabPageMotors = new System.Windows.Forms.TabPage();
            this.motorsTab = new ev3CPanel.MotorsTab();
            this.tabPageSensors = new System.Windows.Forms.TabPage();
            this.sensorsTab = new ev3CPanel.SensorsTab();
            this.tabPageFiles = new System.Windows.Forms.TabPage();
            this.filesTab = new ev3CPanel.FilesTab();
            this.tabPageGpio = new System.Windows.Forms.TabPage();
            this.gpioTab = new ev3CPanel.GpioTab();
            this.gpioTab1 = new ev3CPanel.GpioTab();
            this.backgroundWorkerDisconnect = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerConnectJoystick = new System.ComponentModel.BackgroundWorker();
            this.tabControlRobot.SuspendLayout();
            this.tabPageRobot.SuspendLayout();
            this.groupBoxLightSwitch.SuspendLayout();
            this.groupBoxSensors.SuspendLayout();
            this.groupBoxMotors.SuspendLayout();
            this.groupBoxJoystick.SuspendLayout();
            this.tabPageMotors.SuspendLayout();
            this.tabPageSensors.SuspendLayout();
            this.tabPageFiles.SuspendLayout();
            this.tabPageGpio.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonConnect
            // 
            this.buttonConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConnect.Location = new System.Drawing.Point(6, 10);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(106, 28);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "button1";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // labelConnection
            // 
            this.labelConnection.AutoSize = true;
            this.labelConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnection.Location = new System.Drawing.Point(118, 14);
            this.labelConnection.Name = "labelConnection";
            this.labelConnection.Size = new System.Drawing.Size(123, 20);
            this.labelConnection.TabIndex = 1;
            this.labelConnection.Text = "labelConnection";
            // 
            // backgroundWorkerConnection
            // 
            this.backgroundWorkerConnection.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerConnection_DoWork);
            this.backgroundWorkerConnection.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerConnection_RunWorkerCompleted);
            // 
            // tabControlRobot
            // 
            this.tabControlRobot.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlRobot.Controls.Add(this.tabPageRobot);
            this.tabControlRobot.Controls.Add(this.tabPageMotors);
            this.tabControlRobot.Controls.Add(this.tabPageSensors);
            this.tabControlRobot.Controls.Add(this.tabPageFiles);
            this.tabControlRobot.Controls.Add(this.tabPageGpio);
            this.tabControlRobot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlRobot.Location = new System.Drawing.Point(12, 12);
            this.tabControlRobot.Name = "tabControlRobot";
            this.tabControlRobot.SelectedIndex = 0;
            this.tabControlRobot.Size = new System.Drawing.Size(760, 395);
            this.tabControlRobot.TabIndex = 2;
            // 
            // tabPageRobot
            // 
            this.tabPageRobot.Controls.Add(this.groupBoxLightSwitch);
            this.tabPageRobot.Controls.Add(this.groupBoxSensors);
            this.tabPageRobot.Controls.Add(this.labelConnection);
            this.tabPageRobot.Controls.Add(this.groupBoxMotors);
            this.tabPageRobot.Controls.Add(this.buttonConnect);
            this.tabPageRobot.Controls.Add(this.groupBoxJoystick);
            this.tabPageRobot.Location = new System.Drawing.Point(4, 29);
            this.tabPageRobot.Name = "tabPageRobot";
            this.tabPageRobot.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRobot.Size = new System.Drawing.Size(752, 362);
            this.tabPageRobot.TabIndex = 0;
            this.tabPageRobot.Text = "Home";
            this.tabPageRobot.UseVisualStyleBackColor = true;
            // 
            // groupBoxLightSwitch
            // 
            this.groupBoxLightSwitch.Controls.Add(this.checkBoxLightSwitchWelder);
            this.groupBoxLightSwitch.Controls.Add(this.checkBoxLightSwitchCore);
            this.groupBoxLightSwitch.Controls.Add(this.checkBoxLightSwitchPwrMain);
            this.groupBoxLightSwitch.Controls.Add(this.checkBoxLightSwitchPwr2);
            this.groupBoxLightSwitch.Controls.Add(this.checkBoxLightSwitchPwr1);
            this.groupBoxLightSwitch.Controls.Add(this.checkBoxLightSwitchCtrlTwr);
            this.groupBoxLightSwitch.Location = new System.Drawing.Point(340, 251);
            this.groupBoxLightSwitch.Name = "groupBoxLightSwitch";
            this.groupBoxLightSwitch.Size = new System.Drawing.Size(409, 105);
            this.groupBoxLightSwitch.TabIndex = 26;
            this.groupBoxLightSwitch.TabStop = false;
            this.groupBoxLightSwitch.Text = "Light Switches";
            // 
            // checkBoxLightSwitchWelder
            // 
            this.checkBoxLightSwitchWelder.AutoSize = true;
            this.checkBoxLightSwitchWelder.Location = new System.Drawing.Point(21, 78);
            this.checkBoxLightSwitchWelder.Name = "checkBoxLightSwitchWelder";
            this.checkBoxLightSwitchWelder.Size = new System.Drawing.Size(77, 25);
            this.checkBoxLightSwitchWelder.TabIndex = 15;
            this.checkBoxLightSwitchWelder.Text = "Welder";
            this.checkBoxLightSwitchWelder.UseCompatibleTextRendering = true;
            this.checkBoxLightSwitchWelder.UseVisualStyleBackColor = true;
            this.checkBoxLightSwitchWelder.CheckedChanged += new System.EventHandler(this.checkBoxLightSwitchWelder_CheckedChanged);
            // 
            // checkBoxLightSwitchCore
            // 
            this.checkBoxLightSwitchCore.AutoSize = true;
            this.checkBoxLightSwitchCore.Location = new System.Drawing.Point(21, 52);
            this.checkBoxLightSwitchCore.Name = "checkBoxLightSwitchCore";
            this.checkBoxLightSwitchCore.Size = new System.Drawing.Size(123, 25);
            this.checkBoxLightSwitchCore.TabIndex = 14;
            this.checkBoxLightSwitchCore.Text = "Reactor Core";
            this.checkBoxLightSwitchCore.UseCompatibleTextRendering = true;
            this.checkBoxLightSwitchCore.UseVisualStyleBackColor = true;
            this.checkBoxLightSwitchCore.CheckedChanged += new System.EventHandler(this.checkBoxLightSwitchCore_CheckedChanged);
            // 
            // checkBoxLightSwitchPwrMain
            // 
            this.checkBoxLightSwitchPwrMain.AutoSize = true;
            this.checkBoxLightSwitchPwrMain.Location = new System.Drawing.Point(210, 25);
            this.checkBoxLightSwitchPwrMain.Name = "checkBoxLightSwitchPwrMain";
            this.checkBoxLightSwitchPwrMain.Size = new System.Drawing.Size(112, 25);
            this.checkBoxLightSwitchPwrMain.TabIndex = 13;
            this.checkBoxLightSwitchPwrMain.Text = "Power Main";
            this.checkBoxLightSwitchPwrMain.UseCompatibleTextRendering = true;
            this.checkBoxLightSwitchPwrMain.UseVisualStyleBackColor = true;
            this.checkBoxLightSwitchPwrMain.CheckedChanged += new System.EventHandler(this.checkBoxLightSwitchPwrMain_CheckedChanged);
            // 
            // checkBoxLightSwitchPwr2
            // 
            this.checkBoxLightSwitchPwr2.AutoSize = true;
            this.checkBoxLightSwitchPwr2.Location = new System.Drawing.Point(210, 78);
            this.checkBoxLightSwitchPwr2.Name = "checkBoxLightSwitchPwr2";
            this.checkBoxLightSwitchPwr2.Size = new System.Drawing.Size(134, 25);
            this.checkBoxLightSwitchPwr2.TabIndex = 12;
            this.checkBoxLightSwitchPwr2.Text = "Power Lower 2";
            this.checkBoxLightSwitchPwr2.UseCompatibleTextRendering = true;
            this.checkBoxLightSwitchPwr2.UseVisualStyleBackColor = true;
            this.checkBoxLightSwitchPwr2.CheckedChanged += new System.EventHandler(this.checkBoxLightSwitchPwr2_CheckedChanged);
            // 
            // checkBoxLightSwitchPwr1
            // 
            this.checkBoxLightSwitchPwr1.AutoSize = true;
            this.checkBoxLightSwitchPwr1.Location = new System.Drawing.Point(210, 52);
            this.checkBoxLightSwitchPwr1.Name = "checkBoxLightSwitchPwr1";
            this.checkBoxLightSwitchPwr1.Size = new System.Drawing.Size(134, 25);
            this.checkBoxLightSwitchPwr1.TabIndex = 11;
            this.checkBoxLightSwitchPwr1.Text = "Power Lower 1";
            this.checkBoxLightSwitchPwr1.UseCompatibleTextRendering = true;
            this.checkBoxLightSwitchPwr1.UseVisualStyleBackColor = true;
            this.checkBoxLightSwitchPwr1.CheckedChanged += new System.EventHandler(this.checkBoxLightSwitchPwr1_CheckedChanged);
            // 
            // checkBoxLightSwitchCtrlTwr
            // 
            this.checkBoxLightSwitchCtrlTwr.AutoSize = true;
            this.checkBoxLightSwitchCtrlTwr.Location = new System.Drawing.Point(21, 25);
            this.checkBoxLightSwitchCtrlTwr.Name = "checkBoxLightSwitchCtrlTwr";
            this.checkBoxLightSwitchCtrlTwr.Size = new System.Drawing.Size(126, 25);
            this.checkBoxLightSwitchCtrlTwr.TabIndex = 10;
            this.checkBoxLightSwitchCtrlTwr.Text = "Control Room";
            this.checkBoxLightSwitchCtrlTwr.UseCompatibleTextRendering = true;
            this.checkBoxLightSwitchCtrlTwr.UseVisualStyleBackColor = true;
            this.checkBoxLightSwitchCtrlTwr.CheckedChanged += new System.EventHandler(this.checkBoxLightSwitchCtrlTwr_CheckedChanged);
            // 
            // groupBoxSensors
            // 
            this.groupBoxSensors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSensors.Controls.Add(this.checkBoxTouchLight);
            this.groupBoxSensors.Controls.Add(this.checkBoxIrLights);
            this.groupBoxSensors.Controls.Add(this.checkBoxColorLights);
            this.groupBoxSensors.Controls.Add(this.checkBoxMonitorTouch);
            this.groupBoxSensors.Controls.Add(this.checkBoxMonitorIr);
            this.groupBoxSensors.Controls.Add(this.labelSensorStatus);
            this.groupBoxSensors.Controls.Add(this.checkBoxMonitorColorSensor);
            this.groupBoxSensors.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxSensors.Location = new System.Drawing.Point(340, 44);
            this.groupBoxSensors.Name = "groupBoxSensors";
            this.groupBoxSensors.Size = new System.Drawing.Size(406, 201);
            this.groupBoxSensors.TabIndex = 2;
            this.groupBoxSensors.TabStop = false;
            this.groupBoxSensors.Text = "Sensors";
            // 
            // checkBoxTouchLight
            // 
            this.checkBoxTouchLight.AutoSize = true;
            this.checkBoxTouchLight.Location = new System.Drawing.Point(199, 71);
            this.checkBoxTouchLight.Name = "checkBoxTouchLight";
            this.checkBoxTouchLight.Size = new System.Drawing.Size(69, 25);
            this.checkBoxTouchLight.TabIndex = 9;
            this.checkBoxTouchLight.Text = "Lights";
            this.checkBoxTouchLight.UseCompatibleTextRendering = true;
            this.checkBoxTouchLight.UseVisualStyleBackColor = true;
            this.checkBoxTouchLight.CheckedChanged += new System.EventHandler(this.checkBoxTouchLight_CheckedChanged);
            // 
            // checkBoxIrLights
            // 
            this.checkBoxIrLights.AutoSize = true;
            this.checkBoxIrLights.Location = new System.Drawing.Point(199, 47);
            this.checkBoxIrLights.Name = "checkBoxIrLights";
            this.checkBoxIrLights.Size = new System.Drawing.Size(69, 25);
            this.checkBoxIrLights.TabIndex = 8;
            this.checkBoxIrLights.Text = "Lights";
            this.checkBoxIrLights.UseCompatibleTextRendering = true;
            this.checkBoxIrLights.UseVisualStyleBackColor = true;
            this.checkBoxIrLights.CheckedChanged += new System.EventHandler(this.checkBoxIrLights_CheckedChanged);
            // 
            // checkBoxColorLights
            // 
            this.checkBoxColorLights.AutoSize = true;
            this.checkBoxColorLights.Location = new System.Drawing.Point(199, 22);
            this.checkBoxColorLights.Name = "checkBoxColorLights";
            this.checkBoxColorLights.Size = new System.Drawing.Size(69, 25);
            this.checkBoxColorLights.TabIndex = 7;
            this.checkBoxColorLights.Text = "Lights";
            this.checkBoxColorLights.UseCompatibleTextRendering = true;
            this.checkBoxColorLights.UseVisualStyleBackColor = true;
            this.checkBoxColorLights.CheckedChanged += new System.EventHandler(this.checkBoxColorLights_CheckedChanged);
            // 
            // checkBoxMonitorTouch
            // 
            this.checkBoxMonitorTouch.AutoSize = true;
            this.checkBoxMonitorTouch.Location = new System.Drawing.Point(16, 72);
            this.checkBoxMonitorTouch.Name = "checkBoxMonitorTouch";
            this.checkBoxMonitorTouch.Size = new System.Drawing.Size(129, 24);
            this.checkBoxMonitorTouch.TabIndex = 6;
            this.checkBoxMonitorTouch.Text = "Monitor Touch";
            this.checkBoxMonitorTouch.UseVisualStyleBackColor = true;
            this.checkBoxMonitorTouch.CheckedChanged += new System.EventHandler(this.checkBoxMonitorTouch_CheckedChanged);
            // 
            // checkBoxMonitorIr
            // 
            this.checkBoxMonitorIr.AutoSize = true;
            this.checkBoxMonitorIr.Location = new System.Drawing.Point(16, 48);
            this.checkBoxMonitorIr.Name = "checkBoxMonitorIr";
            this.checkBoxMonitorIr.Size = new System.Drawing.Size(102, 24);
            this.checkBoxMonitorIr.TabIndex = 5;
            this.checkBoxMonitorIr.Text = "Monitor IR";
            this.checkBoxMonitorIr.UseVisualStyleBackColor = true;
            this.checkBoxMonitorIr.CheckedChanged += new System.EventHandler(this.checkBoxMonitorIr_CheckedChanged);
            // 
            // labelSensorStatus
            // 
            this.labelSensorStatus.AutoSize = true;
            this.labelSensorStatus.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSensorStatus.Location = new System.Drawing.Point(13, 102);
            this.labelSensorStatus.Name = "labelSensorStatus";
            this.labelSensorStatus.Size = new System.Drawing.Size(144, 16);
            this.labelSensorStatus.TabIndex = 2;
            this.labelSensorStatus.Text = "labelSensorStatus";
            // 
            // checkBoxMonitorColorSensor
            // 
            this.checkBoxMonitorColorSensor.AutoSize = true;
            this.checkBoxMonitorColorSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonitorColorSensor.Location = new System.Drawing.Point(16, 22);
            this.checkBoxMonitorColorSensor.Name = "checkBoxMonitorColorSensor";
            this.checkBoxMonitorColorSensor.Size = new System.Drawing.Size(131, 24);
            this.checkBoxMonitorColorSensor.TabIndex = 1;
            this.checkBoxMonitorColorSensor.Text = "Monitor Colour";
            this.checkBoxMonitorColorSensor.UseVisualStyleBackColor = true;
            this.checkBoxMonitorColorSensor.CheckedChanged += new System.EventHandler(this.checkBoxMonitorColorSensor_CheckedChanged);
            // 
            // groupBoxMotors
            // 
            this.groupBoxMotors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxMotors.Controls.Add(this.checkBoxMonitorMotorsTach);
            this.groupBoxMotors.Controls.Add(this.labelMotorStatus);
            this.groupBoxMotors.Controls.Add(this.checkBoxMonitorMotorsSpeed);
            this.groupBoxMotors.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxMotors.Location = new System.Drawing.Point(6, 44);
            this.groupBoxMotors.Name = "groupBoxMotors";
            this.groupBoxMotors.Size = new System.Drawing.Size(316, 246);
            this.groupBoxMotors.TabIndex = 1;
            this.groupBoxMotors.TabStop = false;
            this.groupBoxMotors.Text = "Motors";
            // 
            // checkBoxMonitorMotorsTach
            // 
            this.checkBoxMonitorMotorsTach.AutoSize = true;
            this.checkBoxMonitorMotorsTach.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonitorMotorsTach.Location = new System.Drawing.Point(7, 52);
            this.checkBoxMonitorMotorsTach.Name = "checkBoxMonitorMotorsTach";
            this.checkBoxMonitorMotorsTach.Size = new System.Drawing.Size(120, 24);
            this.checkBoxMonitorMotorsTach.TabIndex = 2;
            this.checkBoxMonitorMotorsTach.Text = "Monitor Tach";
            this.checkBoxMonitorMotorsTach.UseVisualStyleBackColor = true;
            this.checkBoxMonitorMotorsTach.CheckedChanged += new System.EventHandler(this.checkBoxMonitorMotorsTach_CheckedChanged);
            // 
            // labelMotorStatus
            // 
            this.labelMotorStatus.AutoSize = true;
            this.labelMotorStatus.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotorStatus.Location = new System.Drawing.Point(4, 85);
            this.labelMotorStatus.Name = "labelMotorStatus";
            this.labelMotorStatus.Size = new System.Drawing.Size(136, 16);
            this.labelMotorStatus.TabIndex = 1;
            this.labelMotorStatus.Text = "labelMotorStatus";
            // 
            // checkBoxMonitorMotorsSpeed
            // 
            this.checkBoxMonitorMotorsSpeed.AutoSize = true;
            this.checkBoxMonitorMotorsSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonitorMotorsSpeed.Location = new System.Drawing.Point(7, 26);
            this.checkBoxMonitorMotorsSpeed.Name = "checkBoxMonitorMotorsSpeed";
            this.checkBoxMonitorMotorsSpeed.Size = new System.Drawing.Size(132, 24);
            this.checkBoxMonitorMotorsSpeed.TabIndex = 0;
            this.checkBoxMonitorMotorsSpeed.Text = "Monitor Speed";
            this.checkBoxMonitorMotorsSpeed.UseVisualStyleBackColor = true;
            this.checkBoxMonitorMotorsSpeed.CheckedChanged += new System.EventHandler(this.checkBoxMonitorMotors_CheckedChanged);
            // 
            // groupBoxJoystick
            // 
            this.groupBoxJoystick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxJoystick.Controls.Add(this.buttonRefresh);
            this.groupBoxJoystick.Controls.Add(this.buttonConnectJoystick);
            this.groupBoxJoystick.Controls.Add(this.comboBoxJoystickPaths);
            this.groupBoxJoystick.Location = new System.Drawing.Point(6, 296);
            this.groupBoxJoystick.Name = "groupBoxJoystick";
            this.groupBoxJoystick.Size = new System.Drawing.Size(322, 60);
            this.groupBoxJoystick.TabIndex = 25;
            this.groupBoxJoystick.TabStop = false;
            this.groupBoxJoystick.Text = "Joystick";
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.Location = new System.Drawing.Point(200, 30);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(77, 24);
            this.buttonRefresh.TabIndex = 19;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonConnectJoystick
            // 
            this.buttonConnectJoystick.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConnectJoystick.Location = new System.Drawing.Point(8, 29);
            this.buttonConnectJoystick.Name = "buttonConnectJoystick";
            this.buttonConnectJoystick.Size = new System.Drawing.Size(102, 24);
            this.buttonConnectJoystick.TabIndex = 0;
            this.buttonConnectJoystick.Text = "Connect";
            this.buttonConnectJoystick.UseVisualStyleBackColor = true;
            this.buttonConnectJoystick.Click += new System.EventHandler(this.buttonConnectJoystick_Click);
            // 
            // comboBoxJoystickPaths
            // 
            this.comboBoxJoystickPaths.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxJoystickPaths.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxJoystickPaths.FormattingEnabled = true;
            this.comboBoxJoystickPaths.Location = new System.Drawing.Point(116, 30);
            this.comboBoxJoystickPaths.Name = "comboBoxJoystickPaths";
            this.comboBoxJoystickPaths.Size = new System.Drawing.Size(78, 24);
            this.comboBoxJoystickPaths.TabIndex = 18;
            // 
            // tabPageMotors
            // 
            this.tabPageMotors.Controls.Add(this.motorsTab);
            this.tabPageMotors.Location = new System.Drawing.Point(4, 22);
            this.tabPageMotors.Name = "tabPageMotors";
            this.tabPageMotors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMotors.Size = new System.Drawing.Size(192, 74);
            this.tabPageMotors.TabIndex = 1;
            this.tabPageMotors.Text = "Motors";
            this.tabPageMotors.UseVisualStyleBackColor = true;
            // 
            // motorsTab
            // 
            this.motorsTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.motorsTab.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.motorsTab.BackColor = System.Drawing.Color.Transparent;
            this.motorsTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motorsTab.Location = new System.Drawing.Point(4, 8);
            this.motorsTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.motorsTab.Name = "motorsTab";
            this.motorsTab.Size = new System.Drawing.Size(192, 61);
            this.motorsTab.TabIndex = 0;
            // 
            // tabPageSensors
            // 
            this.tabPageSensors.Controls.Add(this.sensorsTab);
            this.tabPageSensors.Location = new System.Drawing.Point(4, 22);
            this.tabPageSensors.Name = "tabPageSensors";
            this.tabPageSensors.Size = new System.Drawing.Size(192, 74);
            this.tabPageSensors.TabIndex = 2;
            this.tabPageSensors.Text = "Sensors";
            this.tabPageSensors.UseVisualStyleBackColor = true;
            // 
            // sensorsTab
            // 
            this.sensorsTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorsTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sensorsTab.Location = new System.Drawing.Point(4, 5);
            this.sensorsTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.sensorsTab.Name = "sensorsTab";
            this.sensorsTab.Size = new System.Drawing.Size(184, 83);
            this.sensorsTab.TabIndex = 0;
            // 
            // tabPageFiles
            // 
            this.tabPageFiles.Controls.Add(this.filesTab);
            this.tabPageFiles.Location = new System.Drawing.Point(4, 22);
            this.tabPageFiles.Name = "tabPageFiles";
            this.tabPageFiles.Size = new System.Drawing.Size(192, 74);
            this.tabPageFiles.TabIndex = 3;
            this.tabPageFiles.Text = "Files";
            this.tabPageFiles.UseVisualStyleBackColor = true;
            // 
            // filesTab
            // 
            this.filesTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filesTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filesTab.Location = new System.Drawing.Point(4, 4);
            this.filesTab.Name = "filesTab";
            this.filesTab.Size = new System.Drawing.Size(185, 67);
            this.filesTab.TabIndex = 0;
            // 
            // tabPageGpio
            // 
            this.tabPageGpio.Controls.Add(this.gpioTab);
            this.tabPageGpio.Controls.Add(this.gpioTab1);
            this.tabPageGpio.Location = new System.Drawing.Point(4, 22);
            this.tabPageGpio.Name = "tabPageGpio";
            this.tabPageGpio.Size = new System.Drawing.Size(192, 74);
            this.tabPageGpio.TabIndex = 4;
            this.tabPageGpio.Text = "GPIO";
            this.tabPageGpio.UseVisualStyleBackColor = true;
            // 
            // gpioTab
            // 
            this.gpioTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpioTab.Location = new System.Drawing.Point(0, 0);
            this.gpioTab.Name = "gpioTab";
            this.gpioTab.Size = new System.Drawing.Size(751, 362);
            this.gpioTab.TabIndex = 1;
            this.gpioTab.Tag = "22";
            // 
            // gpioTab1
            // 
            this.gpioTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpioTab1.Location = new System.Drawing.Point(0, 3);
            this.gpioTab1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gpioTab1.Name = "gpioTab1";
            this.gpioTab1.Size = new System.Drawing.Size(749, 359);
            this.gpioTab1.TabIndex = 0;
            this.gpioTab1.Tag = "22";
            // 
            // backgroundWorkerDisconnect
            // 
            this.backgroundWorkerDisconnect.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerDisconnection_DoWork);
            // 
            // backgroundWorkerConnectJoystick
            // 
            this.backgroundWorkerConnectJoystick.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerConnectJoystick_DoWork);
            this.backgroundWorkerConnectJoystick.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerConnectJoystick_RunWorkerCompleted);
            // 
            // MainForm
            // 
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(784, 419);
            this.Controls.Add(this.tabControlRobot);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "ev3 Control Panel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.tabControlRobot.ResumeLayout(false);
            this.tabPageRobot.ResumeLayout(false);
            this.tabPageRobot.PerformLayout();
            this.groupBoxLightSwitch.ResumeLayout(false);
            this.groupBoxLightSwitch.PerformLayout();
            this.groupBoxSensors.ResumeLayout(false);
            this.groupBoxSensors.PerformLayout();
            this.groupBoxMotors.ResumeLayout(false);
            this.groupBoxMotors.PerformLayout();
            this.groupBoxJoystick.ResumeLayout(false);
            this.tabPageMotors.ResumeLayout(false);
            this.tabPageSensors.ResumeLayout(false);
            this.tabPageFiles.ResumeLayout(false);
            this.tabPageGpio.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Label labelConnection;
        private System.ComponentModel.BackgroundWorker backgroundWorkerConnection;
        private System.Windows.Forms.TabControl tabControlRobot;
        private System.Windows.Forms.TabPage tabPageRobot;
        private System.Windows.Forms.GroupBox groupBoxSensors;
        private System.Windows.Forms.Label labelSensorStatus;
        private System.Windows.Forms.CheckBox checkBoxMonitorColorSensor;
        private System.Windows.Forms.GroupBox groupBoxMotors;
        private System.Windows.Forms.Label labelMotorStatus;
        private System.Windows.Forms.CheckBox checkBoxMonitorMotorsSpeed;
        private System.Windows.Forms.TabPage tabPageMotors;
        private System.ComponentModel.BackgroundWorker backgroundWorkerDisconnect;
        private System.Windows.Forms.CheckBox checkBoxMonitorMotorsTach;
        private MotorsTab motorsTab;
        private System.Windows.Forms.TabPage tabPageSensors;
        private SensorsTab sensorsTab;
        private System.Windows.Forms.TabPage tabPageFiles;
        private FilesTab filesTab;
        private System.ComponentModel.BackgroundWorker backgroundWorkerConnectJoystick;
        private System.Windows.Forms.GroupBox groupBoxJoystick;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonConnectJoystick;
        private System.Windows.Forms.ComboBox comboBoxJoystickPaths;
        private System.Windows.Forms.TabPage tabPageGpio;
        private GpioTab gpioTab1;
        private GpioTab gpioTab;
        private System.Windows.Forms.CheckBox checkBoxMonitorIr;
        private System.Windows.Forms.CheckBox checkBoxMonitorTouch;
        private System.Windows.Forms.CheckBox checkBoxTouchLight;
        private System.Windows.Forms.CheckBox checkBoxIrLights;
        private System.Windows.Forms.CheckBox checkBoxColorLights;
        private System.Windows.Forms.GroupBox groupBoxLightSwitch;
        private System.Windows.Forms.CheckBox checkBoxLightSwitchWelder;
        private System.Windows.Forms.CheckBox checkBoxLightSwitchCore;
        private System.Windows.Forms.CheckBox checkBoxLightSwitchPwrMain;
        private System.Windows.Forms.CheckBox checkBoxLightSwitchPwr2;
        private System.Windows.Forms.CheckBox checkBoxLightSwitchPwr1;
        private System.Windows.Forms.CheckBox checkBoxLightSwitchCtrlTwr;
    }
}