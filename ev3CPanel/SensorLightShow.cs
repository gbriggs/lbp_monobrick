﻿using System;
using System.Collections.Generic;
using MonoBrick.EV3;
using MonoRobot;
using GpioManagerObjects;
using WiringPiWrapper;
using System.Timers;
using System.Linq;
using System.Xml.Linq;
using System.Xml;

namespace ev3CPanel
{
    public class SensorLightShow
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pinManager">reference to the pin manager</param>
        /// <param name="robot">reference to the robot</param>
        public SensorLightShow(GpioManager pinManager, Robot robot)
        {
            PinManager = pinManager;
            TheRobot = robot;
        }

        private GpioManager PinManager;
        private Robot TheRobot;


        public void InitPins()
        {
            //  init the GPIO library (physical pin numbers)
            PinManager.Setup();

         
            //  init all the RPI regular gpio pins
            foreach (var nextIndex in whiteList)
            {
                var newPin = new GpioPinWrapper(nextIndex, nextIndex == 12);
                newPin.Mode = GPIO.GPIOpinmode.Output;
                PinManager.AddPinToManager(newPin);
            }

            //  setup MCP
            PinManager.SetupMcp(MCP.McpType.Mcp23017, 900, 33);
            for (int i = 900; i < 916; i++)
            {
                var newPin = new GpioPinWrapper(i, false);
                newPin.Mode = GPIO.GPIOpinmode.Output;
                PinManager.AddPinToManager(newPin);
            }

            //  setup PCA
            PinManager.SetupPca(800, 65, 50);
            for (int i = 800; i < 816; i++)
            {
                var newPin = new GpioPinWrapperPca(i);
                PinManager.AddPinToManager(newPin);
                newPin.Mode = GPIO.GPIOpinmode.PWMOutput;
                newPin.PwmSetValue(0.0);
            }

            //  Setup IR sensor seven segment display
            string segDisplay1 = @"<SevenSegDisplay>
                <SegPins>904,905,906,907,908,909,910,911</SegPins>
                <Digits>-902,-901,-900</Digits>
                </SevenSegDisplay>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(segDisplay1);

            IrSensorSevenSegDisplay = PinManager.CreateSevenSegDisplayDriver(doc.FirstChild);
            IrCurrentMode = IRMode.Proximity;
            IrCurrentDisplay = "";

            //  IR mode pins
            IrProximityModePin = PinManager.GetPin(914);
            AllPins.Add(IrProximityModePin);
            IrRemoteModePin = PinManager.GetPin(913);
            AllPins.Add(IrRemoteModePin);
            IrSeekModePin = PinManager.GetPin(912);
            AllPins.Add(IrSeekModePin);


            //  Touch pin
            TouchPin = PinManager.GetPin(809);
            AllPins.Add(TouchPin);


            //  Color
            ColorRedPin = PinManager.GetPin(815);
            AllPins.Add(ColorRedPin);
            ColorGreenPin = PinManager.GetPin(814);
            AllPins.Add(ColorGreenPin);
            ColorBluePin = PinManager.GetPin(813);
            AllPins.Add(ColorBluePin);

            //  set lights to off
            ColorRedPin.Write(1);
            ColorGreenPin.Write(1);
            ColorBluePin.Write(1);

            ColorModeAmbientPin = PinManager.GetPin(808);
            AllPins.Add(ColorModeAmbientPin);
            ColorModeColorPin = PinManager.GetPin(807);
            AllPins.Add(ColorModeColorPin);
            ColorModeNxtPin = PinManager.GetPin(806);
            AllPins.Add(ColorModeNxtPin);
            ColorModeReflectionPin = PinManager.GetPin(805);
            AllPins.Add(ColorModeReflectionPin);

            ColorLevel0Pin = PinManager.GetPin(800);
            AllPins.Add(ColorLevel0Pin);
            ColorLevel1Pin = PinManager.GetPin(801);
            AllPins.Add(ColorLevel1Pin);
            ColorLevel2Pin = PinManager.GetPin(802);
            AllPins.Add(ColorLevel2Pin);
            ColorLevel3Pin = PinManager.GetPin(803);
            AllPins.Add(ColorLevel3Pin);
            ColorLevel4Pin = PinManager.GetPin(804);
            AllPins.Add(ColorLevel4Pin);

            //  Left, Right Track
            DriverLeftPin = PinManager.GetPin(810);
            DriverRightPin = PinManager.GetPin(811);

            //  setup the heartbeat timer
            HeartbeatPin = PinManager.GetPin(812);
            AllPins.Add(HeartbeatPin);
            HeartbeatOn = false;
            HeartbeatTimer = new Timer(100);
            HeartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
            HeartbeatTimer.Start();

            //  setup the welder timer
            WelderPin = PinManager.GetPin(40);
            WelderTimer = new Timer(10000);
            WelderTimer.Elapsed += WelderTimer_Elapsed;
            WelderTimer.Start();

            LightCmdCenterMain = PinManager.GetPin(31);
            LightPwrMain = PinManager.GetPin(38);
            LightPwrLower1 = PinManager.GetPin(37);
            LightPwrLower2 = PinManager.GetPin(36);
        }

       

        //  List of all pins in the light show
        protected List<GpioPinWrapper> AllPins = new List<GpioPinWrapper>();

        //  list of pins from the raspberry pi hardware we will control
        protected int[] whiteList = new int[17] { 7, 11, 12, 13, 15, 16, 18, 22, 29, 31, 32, 33, 35, 36, 37, 38, 40 };


        /// <summary>
        /// All off
        /// </summary>
        public void AllOff()
        {
            //  turn off the accessories
            HeartbeatOn = false;
            WelderOn = false;
           
            //  turn all the pins off
            foreach ( var nextPin in AllPins )
            {
                nextPin.Write(0);
            }

            //  turn color rgb to on high to set the light off
            ColorRedPin.Write(1);
            ColorGreenPin.Write(1);
            ColorBluePin.Write(1);

            //  turn off the seven segment display
            GpioManager.SevenSegmentDisplayOff(IrSensorSevenSegDisplay.DisplayIndex);
        }
     
      


        //  IR Sensor
        SevenSegDisplayWrapper IrSensorSevenSegDisplay;
        //
        GpioPinWrapper IrProximityModePin;
        GpioPinWrapper IrRemoteModePin;
        GpioPinWrapper IrSeekModePin;
        //
        IRMode IrCurrentMode;
        string IrCurrentDisplay;


        //  Touch Sensor
        GpioPinWrapper TouchPin;

        //  Color Pins
        GpioPinWrapper ColorRedPin;
        GpioPinWrapper ColorGreenPin;
        GpioPinWrapper ColorBluePin;

        GpioPinWrapper ColorModeAmbientPin;
        GpioPinWrapper ColorModeColorPin;
        GpioPinWrapper ColorModeNxtPin;
        GpioPinWrapper ColorModeReflectionPin;

        GpioPinWrapper ColorLevel0Pin;
        GpioPinWrapper ColorLevel1Pin;
        GpioPinWrapper ColorLevel2Pin;
        GpioPinWrapper ColorLevel3Pin;
        GpioPinWrapper ColorLevel4Pin;

        //  
        ColorMode ColorCurrentMode;
        string ColorCurrentColorDisplay = "None";

        //  Pin assignments


       

        //  Touch Pin
        public int TouchLightPin = 914;

       
    
        bool FlashState = false;
        int TimeOfLastFlash = 0;
        bool Flash()
        {
            if (Environment.TickCount - TimeOfLastFlash > 667)
            {
                FlashState = !FlashState;
                TimeOfLastFlash = Environment.TickCount;
            }
            return FlashState;
        }

        /// <summary>
        /// Set lights for IR mode
        /// </summary>
        public void SetLightForIr()
        {
            switch (TheRobot.RobotIrSensor.Mode)
            {
                case IRMode.Proximity:
                    SetLightsForIrProximityMode();
                    break;

                case IRMode.Remote:
                    SetLightsForIrRemoteMode();
                    break;

                case IRMode.Seek:
                    SetLightsForIrSeekMode();
                    break;
            }

        }


        /// <summary>
        /// Set lights for IR proximity mode
        /// </summary>
        void SetLightsForIrProximityMode()
        {
            if ( IrCurrentDisplay == "" || IrCurrentMode != IRMode.Proximity )
            {
                IrCurrentMode = IRMode.Proximity;
                IrProximityModePin.Write(1);
                IrRemoteModePin.Write(0);
                IrSeekModePin.Write(0);

            }

            string reading = TheRobot.RobotIrSensor.LatestReading;
            if (reading.CompareTo(IrCurrentDisplay) != 0)
            {
                GpioManager.SevenSegmentDisplaySetDisplay(IrSensorSevenSegDisplay.DisplayIndex, reading);
                IrCurrentDisplay = reading;
            }

           
        }


        /// <summary>
        /// Set lights for IR remote mode
        /// </summary>
        void SetLightsForIrRemoteMode()
        {
            if (IrCurrentDisplay == "" || IrCurrentMode != IRMode.Remote)
            {
                IrCurrentMode = IRMode.Remote;
                IrProximityModePin.Write(0);
                IrRemoteModePin.Write(1);
                IrSeekModePin.Write(0);

            }

            string reading = TheRobot.RobotIrSensor.LatestReading;
            if (reading.CompareTo(IrCurrentDisplay) != 0)
            {
                GpioManager.SevenSegmentDisplaySetDisplay(IrSensorSevenSegDisplay.DisplayIndex, reading);
                IrCurrentDisplay = reading;
            }

        }


        /// <summary>
        /// Set lights for IR seek mode
        /// </summary>
        void SetLightsForIrSeekMode()
        {
            if (IrCurrentDisplay == "" || IrCurrentMode != IRMode.Seek)
            {
                IrCurrentMode = IRMode.Seek;
                IrProximityModePin.Write(0);
                IrRemoteModePin.Write(0);
                IrSeekModePin.Write(1);

            }

            string reading = TheRobot.RobotIrSensor.LatestReading;
            if (reading.CompareTo(IrCurrentDisplay) != 0)
            {
                GpioManager.SevenSegmentDisplaySetDisplay(IrSensorSevenSegDisplay.DisplayIndex, reading);
                IrCurrentDisplay = reading;
            }
        }


        /// <summary>
        /// Turn Ir Off
        /// </summary>
        public void IrOff()
        {
            IrProximityModePin.Write(0);
            IrRemoteModePin.Write(0);
            IrSeekModePin.Write(0);

            GpioManager.SevenSegmentDisplayOff(IrSensorSevenSegDisplay.DisplayIndex);
        }



        /// <summary>
        /// Set lights for touch mode
        /// </summary>
        int TouchCounter = 0;
        public void SetLightForTouch()
        {
            if (TheRobot.MonitorTouchSensor)
            {
                switch (TheRobot.RobotTouchSensor.Mode)
                {
                    case TouchMode.Boolean:
                        {
                            switch (TheRobot.RobotTouchSensor.LatestReading)
                            {
                                case "On":
                                    TouchPin.Write(1);
                                    break;

                                case "Off":
                                    TouchPin.Write(0);
                                    break;

                                default:
                                    break;
                            }
                        }
                        break;

                    case TouchMode.Count:
                        {
                            try
                            {
                                string readingString = TheRobot.RobotTouchSensor.LatestReading;
                                var split = readingString.Split(' ');
                                if (split.Length == 0)
                                    return;

                                int reading;
                                Int32.TryParse(split[0], out reading);
                                if (reading > TouchCounter)
                                {
                                    TouchPin.Write(1);
                                    TouchCounter = reading;
                                }
                                else
                                {
                                    TouchPin.Write(0);
                                }
                            }
                            catch (Exception e)
                            {
                                System.Console.WriteLine("! SetLightsForTouch : " + e.ToString());
                            }
                        }
                        break;
                }
            }
        }


        /// <summary>
        /// Turn touch off
        /// </summary>
        public void TouchOff()
        {
            TouchPin.Write(0);
        }


        /// <summary>
        /// Set lights for color mode
        /// </summary>
        public void SetLightForColor()
        {
            switch (TheRobot.RobotColorSensor.Mode)
            {
                case ColorMode.Reflection:
                case ColorMode.Ambient:
                    SetLightsForColorAmbientReflectMode();
                    break;

                case ColorMode.Color:
                    SetLightsForColorColorMode();
                    break;

                case ColorMode.Raw:
                    SetLightsForColorNXT();
                    break;

                default:
                    //  not supported ?
                    break;
            }

        }

        


        /// <summary>
        /// Set color lights for color mode
        /// </summary>
        void SetLightsForColorColorMode()
        {
            if (ColorCurrentMode != ColorMode.Color)
            {
                ColorCurrentMode = ColorMode.Color;
                ColorModeAmbientPin.Write(0);
                ColorModeColorPin.PwmSetValue(.05);
                ColorModeNxtPin.Write(0);
                ColorModeReflectionPin.Write(0);
            }
            
            switch (this.TheRobot.RobotColorSensor.LatestReading)
            {
                default:
                case "None":
                    if (ColorCurrentColorDisplay == "None")
                        return;
                    ColorCurrentColorDisplay = "None";
                    ColorRedPin.PwmSetValue(1.0);
                    ColorGreenPin.PwmSetValue(1.0);
                    ColorBluePin.PwmSetValue(1.0);
                    break;

                case "Brown":
                    if (ColorCurrentColorDisplay == "Brown")
                        return;
                    ColorCurrentColorDisplay = "Brown";
                    ColorRedPin.PwmSetValue((255.0-139.0) / 255.0);
                    ColorGreenPin.PwmSetValue((255.0 - 69.0) / 255.0);
                    ColorBluePin.PwmSetValue((255.0 - 19.0) / 255.0);
                    break;

                case "White":
                    if (ColorCurrentColorDisplay == "White")
                        return;
                    ColorCurrentColorDisplay = "White";
                    ColorRedPin.PwmSetValue(0.0);
                    ColorGreenPin.PwmSetValue(0.0);
                    ColorBluePin.PwmSetValue(0.0);
                    break;

                case "Red":
                    if (ColorCurrentColorDisplay == "Red")
                        return;
                    ColorCurrentColorDisplay = "Red";
                    ColorRedPin.PwmSetValue(0.0);
                    ColorGreenPin.PwmSetValue(1.0);
                    ColorBluePin.PwmSetValue(1.0);
                    break;

                case "Yellow":
                    if (ColorCurrentColorDisplay == "Yellow")
                        return;
                    ColorCurrentColorDisplay = "Yellow";
                    ColorRedPin.PwmSetValue(0.0);
                    ColorGreenPin.PwmSetValue(0.0);
                    ColorBluePin.PwmSetValue(1.0);
                    break;

                case "Green":
                    if (ColorCurrentColorDisplay == "Green")
                        return;
                    ColorCurrentColorDisplay = "Green";
                    ColorRedPin.PwmSetValue(1.0);
                    ColorGreenPin.PwmSetValue(0.0);
                    ColorBluePin.PwmSetValue(1.0);
                    break;


                case "Blue":
                    if (ColorCurrentColorDisplay == "Blue")
                        return;
                    ColorCurrentColorDisplay = "Blue";
                    ColorRedPin.PwmSetValue(1.0);
                    ColorGreenPin.PwmSetValue(1.0);
                    ColorBluePin.PwmSetValue(0.0);
                    break;


                case "Black":
                    
                    ColorCurrentColorDisplay = "Black";
                    bool flash = Flash();
                    ColorRedPin.PwmSetValue(flash ? 1.0 : 0.0);
                    ColorGreenPin.PwmSetValue(flash ? 1.0 : 0.0);
                    ColorBluePin.PwmSetValue(flash ? 1.0 : 0.0);
                    break;
            }
        }


        /// <summary>
        /// Set lights for color ambient or reflection mode
        /// </summary>
        void SetLightsForColorAmbientReflectMode()
        {
            switch (TheRobot.RobotColorSensor.Mode)
            {
                case ColorMode.Reflection:
                    {
                        if (ColorCurrentMode != ColorMode.Reflection)
                        {
                            ColorCurrentMode = ColorMode.Reflection;
                            ColorModeAmbientPin.Write(0);
                            ColorModeColorPin.Write(0);
                            ColorModeNxtPin.Write(0);
                            ColorModeReflectionPin.PwmSetValue(.1);
                        }
                    }
                    break;
                case ColorMode.Ambient:
                    {
                        if (ColorCurrentMode != ColorMode.Ambient)
                        {
                            ColorCurrentMode = ColorMode.Ambient;
                            ColorModeAmbientPin.PwmSetValue(.05);
                            ColorModeColorPin.Write(0);
                            ColorModeNxtPin.Write(0);
                            ColorModeReflectionPin.Write(0);
                        }
                    }
                    break;
            }

            try
            {
                double reading;
                double.TryParse(TheRobot.RobotColorSensor.LatestReading, out reading);

                int SwitchValue = (int)  ( reading / 10 );
                if (reading <= 1.0)
                    SwitchValue = 0;
                if (reading >= 99)
                    SwitchValue = 10;
                switch (SwitchValue)
                {
                    case 0:
                        ColorLevel0Pin.Write(0);
                        ColorLevel1Pin.Write(0);
                        ColorLevel2Pin.Write(0);
                        ColorLevel3Pin.Write(0);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 1:
                        ColorLevel0Pin.PwmSetValue(0.025);
                        ColorLevel1Pin.Write(0);
                        ColorLevel2Pin.Write(0);
                        ColorLevel3Pin.Write(0);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 2:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.Write(0);
                        ColorLevel2Pin.Write(0);
                        ColorLevel3Pin.Write(0);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 3:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.025);
                        ColorLevel2Pin.Write(0);
                        ColorLevel3Pin.Write(0);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 4:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.1);
                        ColorLevel2Pin.Write(0);
                        ColorLevel3Pin.Write(0);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 5:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.1);
                        ColorLevel2Pin.PwmSetValue(0.025);
                        ColorLevel3Pin.Write(0);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 6:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.1);
                        ColorLevel2Pin.PwmSetValue(0.1);
                        ColorLevel3Pin.Write(0);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 7:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.1);
                        ColorLevel2Pin.PwmSetValue(0.1);
                        ColorLevel3Pin.PwmSetValue(0.025);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 8:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.1);
                        ColorLevel2Pin.PwmSetValue(0.1);
                        ColorLevel3Pin.PwmSetValue(0.1);
                        ColorLevel4Pin.Write(0);
                        break;
                    case 9:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.1);
                        ColorLevel2Pin.PwmSetValue(0.1);
                        ColorLevel3Pin.PwmSetValue(0.1);
                        ColorLevel4Pin.PwmSetValue(0.025);
                        break;
                    case 10:
                        ColorLevel0Pin.PwmSetValue(0.1);
                        ColorLevel1Pin.PwmSetValue(0.1);
                        ColorLevel2Pin.PwmSetValue(0.1);
                        ColorLevel3Pin.PwmSetValue(0.1);
                        ColorLevel4Pin.PwmSetValue(0.1);
                        break;
                        
                        
                   
                }



            }
            catch (Exception e)
            {
                Console.WriteLine("! SetLightsForColorAmbientMode : " + e.ToString());

            }

        }


        private void SetLightsForColorNXT()
        {
            if (ColorCurrentMode != ColorMode.Raw)
            {
                ColorCurrentMode = ColorMode.Raw;
                ColorModeAmbientPin.Write(0);
                ColorModeColorPin.Write(0);
                ColorModeNxtPin.PwmSetValue(.1);
                ColorModeReflectionPin.Write(0);
            }
        }


        /// <summary>
        /// Color off
        /// </summary>
        public void ColorOff()
        {
            ColorModeAmbientPin.Write(0);
            ColorModeColorPin.Write(0);
            ColorModeNxtPin.Write(0);
            ColorModeReflectionPin.Write(0);

            ColorLevel0Pin.Write(0);
            ColorLevel1Pin.Write(0);
            ColorLevel2Pin.Write(0);
            ColorLevel3Pin.Write(0);
            ColorLevel4Pin.Write(0);

            ColorRedPin.Write(1);
            ColorGreenPin.Write(1);
            ColorBluePin.Write(1);
        }



        //  Heartbeat 
        Timer HeartbeatTimer;
        GpioPinWrapper HeartbeatPin;
        protected int HeartbeatLevel = 50;
        protected int HeartbeatMin = 150;
        protected int HeartbeatDirection = 1;
        public bool HeartbeatOn { get; protected set; }
        protected int HeartbeatSkip = 0;


        public void EnableHeartbeat(bool enable)
        {
            HeartbeatOn = enable;
            if (!HeartbeatOn)
                HeartbeatPin.Write(0);
        }

        /// <summary>
        /// Heartbeat timer
        /// </summary>
        private void HeartbeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!HeartbeatOn)
                return;

//            //  linger for three seconds in dim mode
//            if (HeartbeatLevel == HeartbeatMin && HeartbeatSkip < 30)
//            {
//                HeartbeatSkip++;
//                return;
//            }
//            else if (HeartbeatLevel == HeartbeatMin)
//                HeartbeatSkip = 0;

            HeartbeatLevel += HeartbeatDirection * 82;
            if (HeartbeatLevel >= 4096)
            {
                HeartbeatLevel = 4096;
                HeartbeatDirection *= -1;
            }
            else if (HeartbeatLevel < HeartbeatMin)
            {

                HeartbeatLevel = HeartbeatMin;
                HeartbeatDirection *= -1;
            }

            HeartbeatPin.PwmSetValue(HeartbeatLevel);
        }


        //  Welder
        Timer WelderTimer;
        GpioPinWrapper WelderPin;
        public bool WelderOn { get; set; }



        private void WelderTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!WelderOn)
                return;

            Random rnd = new Random();
            WelderPin.Write(1);
            System.Threading.Thread.Sleep(rnd.Next(1000, 2000));
            WelderPin.Write(0);
            System.Threading.Thread.Sleep(rnd.Next(200, 500));
            for ( int i = 0; i < 7; i++ )
            {
                WelderPin.Write(1);
                System.Threading.Thread.Sleep(rnd.Next(20, 100));
                WelderPin.Write(0);
                System.Threading.Thread.Sleep(rnd.Next(50, 100));
            }
             
        }


     


        //  Lights
        public GpioPinWrapper LightCmdCenterMain { get; set; }
        public GpioPinWrapper LightPwrMain { get; set; }
        public GpioPinWrapper LightPwrLower1 { get; set; }
        public GpioPinWrapper LightPwrLower2 { get; set; }


        // Driver Pins
        bool DriverOn = true;
        GpioPinWrapper DriverLeftPin;
        GpioPinWrapper DriverRightPin;

        public void DriverOff()
        {
            DriverLeftPin.PwmSetValue(0);
            DriverRightPin.PwmSetValue(0);
        }


        public void SetDriveLights(int left, int right)
        {
            if ( DriverOn )
            {
                left = Math.Abs(left);
                right = Math.Abs(right);
                DriverLeftPin.PwmSetValue((double)left / 100.0);
                DriverRightPin.PwmSetValue((double)right/100.0);
            }
        }
    }
}
