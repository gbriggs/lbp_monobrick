﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using SimpleJoy;
using PlatformHelper;
using MonoBrick.EV3;
using MonoRobot;
using GpioManagerObjects;

namespace ev3CPanel
{



    public partial class MainForm : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MainForm()
        {
            //  Winform initialization function
            InitializeComponent();

          
            //  Create a Robot object, the main form owns the one instance of our robot object
            TheRobot = new MonoRobot.Robot();

            //  hook up robot events           
            TheRobot.ConnectionStateChanged += Robot_ConnectionStateChanged;
            TheRobot.MotorStateChanged += Robot_MotorStateChanged;
            TheRobot.SensorStateChanged += Robot_SensorStateChanged;

            TheRobot.MonitorTouchSensor = true;
            TheRobot.MonitorIrSensor = true;
            TheRobot.MonitorColorSensor = true;
            TheRobot.MonitorMotorSpeed = true;
            TheRobot.MonitorMotorTach = true;

            // Hook up Joystick events
            Joystick.XboxJoystickEventHandler += Joystick_XboxJoystickEventHandler;
    
            //  Init Joystick Control
            if (PlatformHelper.PlatformHelper.RunningPlatform() == Platform.Linux)
            {
                List<string> paths = Joystick.GetJoystickPaths();
                foreach (var nextPath in paths)
                    comboBoxJoystickPaths.Items.Add(nextPath);
                if (paths.Count == 0)
                    comboBoxJoystickPaths.Items.Add(NoneFound);
                comboBoxJoystickPaths.SelectedIndex = 0;
            }
            else
            {
                //  windows has no joystick
                comboBoxJoystickPaths.Visible = false;
                buttonRefresh.Visible = false;
            }

            timeSinceTabSwitch = Environment.TickCount;
            timeSinceDpadSwitch = Environment.TickCount;

            //  instantiate the Pin Manager
            PinManager = new GpioManager();

            //  Initialize the light show
            LightShow = new SensorLightShow(PinManager, TheRobot);
            LightShow.InitPins();

            //  Init Connection UI
            ConnectionUi("");

            //  setup the tab pages
            //  Motors Tab
            motorsTab.InitMotorControl(TheRobot);
            //  Sensors Tab
            sensorsTab.InitSensorControl(TheRobot);
            //  Files tab
            filesTab.InitFileTabPage(TheRobot);
            //  GPIO tab page (for raspberry pi)
            gpioTab.InitializeGpioTab(PinManager);

#if ! RPI
            //  Hide the lights stuff if we are not rpi build
            checkBoxColorLights.Hide();
            checkBoxIrLights.Hide();
            checkBoxTouchLight.Hide();

            groupBoxLightSwitch.Hide();
            checkBoxLightSwitchCtrlTwr.Hide();
            checkBoxLightSwitchCore.Hide();
            checkBoxLightSwitchPwrMain.Hide();
            checkBoxLightSwitchPwr1.Hide();
            checkBoxLightSwitchPwr2.Hide();
            checkBoxLightSwitchWelder.Hide();
            tabControlRobot.TabPages.Remove(tabPageGpio);
#endif

            //  Hide the joystick button if we are on windows
            if ( PlatformHelper.PlatformHelper.RunningPlatform() == Platform.Windows )
            {
                //  hide the joystick controls
                groupBoxJoystick.Visible = false;
                buttonConnectJoystick.Visible = false;
                buttonRefresh.Visible = false;
                comboBoxJoystickPaths.Visible = false;
            }


            //  Motor monitor check boxes and display UI
            checkBoxMonitorMotorsSpeed.Checked = TheRobot.MonitorMotorSpeed;
            checkBoxMonitorMotorsTach.Checked = TheRobot.MonitorMotorTach;
            SensorStatusUpdateUi();

            //  Monitor sensor check boxes and display UI
            checkBoxMonitorColorSensor.Checked = TheRobot.MonitorColorSensor;
            checkBoxMonitorIr.Checked = TheRobot.MonitorIrSensor;
            checkBoxMonitorTouch.Checked = TheRobot.MonitorTouchSensor;
            MotorsStatusUpdateUi();

        }

      

        /// <summary>
        /// Shut down Robot on main form closing
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            TheRobot.ShutDown();

            LightShow.AllOff();

            //  clean up joystick
            if (Joystick.IsConnected)
                Joystick.Disconnect();

        }


        //  The main form will own instances of the component parts of the app
        //

        //  The Robot
        public MonoRobot.Robot TheRobot { get; private set; }

        //  The GPIO Pin Manager
        public GpioManager PinManager { get; protected set; }

        //  Sensor Light show 
        public SensorLightShow LightShow { get; protected set; }
        

        //  The Joystick
        SimpleJoy.SimpleJoystick Joystick = new SimpleJoystick();
        //
        public bool JoyStickConnected { get { return Joystick.IsConnected; } }
        static string NoneFound = "none found";
      

        //  Robot Events that update UI
        //
        /// <summary>
        /// Connection State Event
        /// </summary>
        void Robot_ConnectionStateChanged(object sender, MonoRobot.ConnectionEventArgs e)
        {
            //  Update UI on main thread
            Invoke((MethodInvoker)(() =>
            {
                ConnectionUi(e.Message);
            }));
        }

        /// <summary>
        /// Format connection state into UI
        /// </summary>
        void ConnectionUi(string message)
        {
            buttonConnect.Text = "Connect";
            buttonConnect.Enabled = true;

            switch (TheRobot.ConnectionStatus)
            {
                case MonoRobot.ConnectionState.Connected:
                    buttonConnect.Text = "Disconnect";
                    labelConnection.Text = "Connected"; //  todo conneciton details
                    filesTab.UpdateBrickFiles();
                    break;

                case MonoRobot.ConnectionState.Disconnected:
                    labelConnection.Text = "Disconnected";
                    buttonConnect.Text = "Connect";
                    break;

                case MonoRobot.ConnectionState.Connecting:
                    buttonConnect.Text = "Cancel";
                    buttonConnect.Enabled = false;

                    if (message.Length == 0)
                        labelConnection.Text = "Connecting ...";
                    else
                        labelConnection.Text = message;

                    break;

                case MonoRobot.ConnectionState.Unknown:
                default:
                    labelConnection.Text = "Unknown !";
                    break;
            }
        }




        /// <summary>
        /// Connection Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (TheRobot.ConnectionStatus == MonoRobot.ConnectionState.Connected)
            {
                backgroundWorkerDisconnect.RunWorkerAsync();
            }
            else
            {
#if RPI
                //  Raspberry Pi crashes if you show the connection form
                //  it only works using wifi
                backgroundWorkerConnection.RunWorkerAsync("");
#else
                //  launch the connect form
                ConnectionForm connectForm = new ConnectionForm();

                if (connectForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    backgroundWorkerConnection.RunWorkerAsync(connectForm);
                }
#endif
            }
        }

        /// <summary>
        /// Connect to robot is done in background worker 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">e.Argument is a ConnectionForm object</param>
        private void backgroundWorkerConnection_DoWork(object sender, DoWorkEventArgs e)
        {
#if RPI
            // Raspberry Pi hard wired to wifi only
            TheRobot.ConnectToRobot(MonoRobot.ConnectMethod.WiFi, "");
#else
            //  Connect to the robot with the choice specified on the connection form
            ConnectionForm form = (ConnectionForm)e.Argument;
            TheRobot.ConnectToRobot(form.ConnectMethod, form.Address);
#endif
        }
        //
        private void backgroundWorkerConnection_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            motorsTab.UpdateMotorTrackbarControls();
            sensorsTab.ReinitSensorControls();

        }
        //
        private void backgroundWorkerDisconnection_DoWork(object sender, DoWorkEventArgs e)
        {
            TheRobot.DisconnectFromRobot();
        }





        /// <summary>
        /// Sensor State Event
        /// This will also trigger the light circuts to flash
        /// </summary>
        void Robot_SensorStateChanged(object sender, MonoRobot.SensorEventArgs e)
        {
            //  Update UI on main thread
            Invoke((MethodInvoker)(() =>
            {
                SensorStatusUpdateUi();
            }));

            //  make lights flash
            if (checkBoxColorLights.Checked && TheRobot.MonitorColorSensor)
            {
                LightShow.SetLightForColor();
            }
            if (checkBoxIrLights.Checked && TheRobot.MonitorIrSensor)
            {
                LightShow.SetLightForIr();
            }
            if (checkBoxTouchLight.Checked && TheRobot.MonitorTouchSensor)
            {
                LightShow.SetLightForTouch();
            }
        }

        //  Update Sensor UI text
        void SensorStatusUpdateUi()
        {
            string sensorLabel = "";
            if (TheRobot.IsConnected && TheRobot.MonitorSensors)
            {
                if ( TheRobot.MonitorColorSensor && TheRobot.RobotColorSensor != null)
                {
                    sensorLabel += string.Format("{0} \n", TheRobot.RobotColorSensor.SensorType.ToString());
                    sensorLabel += string.Format("    {0}:     {1} \n", TheRobot.SensorModeName(TheRobot.RobotColorSensor), TheRobot.RobotColorSensor.LatestReading);
                }
                if (TheRobot.MonitorIrSensor && TheRobot.RobotIrSensor != null)
                {
                    sensorLabel += string.Format("{0} \n", TheRobot.RobotIrSensor.SensorType.ToString());
                    sensorLabel += string.Format("    {0}:     {1} \n", TheRobot.SensorModeName(TheRobot.RobotIrSensor), TheRobot.RobotIrSensor.LatestReading);
                }
                if (TheRobot.MonitorTouchSensor && TheRobot.RobotTouchSensor != null)
                {
                    sensorLabel += string.Format("{0} \n", TheRobot.RobotTouchSensor.SensorType.ToString());
                    sensorLabel += string.Format("    {0}:     {1} \n", TheRobot.SensorModeName(TheRobot.RobotTouchSensor), TheRobot.RobotTouchSensor.LatestReading);
                }
            }
            labelSensorStatus.Text = sensorLabel;

            sensorsTab.UpdateSensorControls();
        }


        /// <summary>
        /// Sensor Check Box Functions
        /// </summary>
        /// 

        private void checkBoxMonitorColorSensor_CheckedChanged(object sender, EventArgs e)
        {
            TheRobot.MonitorColorSensor = checkBoxMonitorColorSensor.Checked;
            SensorStatusUpdateUi();
        }

        private void checkBoxMonitorTouch_CheckedChanged(object sender, EventArgs e)
        {
            TheRobot.MonitorTouchSensor = checkBoxMonitorTouch.Checked;
            SensorStatusUpdateUi();
        }

        private void checkBoxMonitorIr_CheckedChanged(object sender, EventArgs e)
        {
            TheRobot.MonitorIrSensor = checkBoxMonitorIr.Checked;
            SensorStatusUpdateUi();
        }

        private void checkBoxColorLights_CheckedChanged(object sender, EventArgs e)
        {
            if ( ! checkBoxColorLights.Checked )
            {
                try
                {
                    LightShow.ColorOff();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("! Failed to turn off colored lights: " + ex.ToString());
                }
            }
        }

        private void checkBoxIrLights_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBoxIrLights.Checked)
            {
                try
                {
                    LightShow.IrOff();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("! Failed to turn off ir lights: " + ex.ToString());
                }
            }
        }

        private void checkBoxTouchLight_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBoxTouchLight.Checked)
            {
                try
                {
                    LightShow.TouchOff();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("! Failed to turn off touch light: " + ex.ToString());
                }
            }
        }

        /// <summary>
        /// Motors State Changed
        /// </summary>
        void Robot_MotorStateChanged(object sender, MonoRobot.MotorEventArgs e)
        {
            //  Update UI on main thread
            Invoke((MethodInvoker)(() =>
            {
                motorsTab.UpdateMotorTrackbarControls();
                MotorsStatusUpdateUi();
            }));
        }
        //
        void MotorsStatusUpdateUi()
        {
            string motorLabel = "";
            if (TheRobot.IsConnected && TheRobot.MonitoringMotors)
            {
                for (int i = 0; i < TheRobot.MotorCount; i++)
                {
                    motorLabel += string.Format("Motor {0}: {3} {1} {2}\n", TheRobot.MotorPort(i), TheRobot.MonitorMotorSpeed ? TheRobot.MotorSpeed(i).ToString() : "--", TheRobot.MonitorMotorTach ? TheRobot.MotorTachoCount(i).ToString() : "--", TheRobot.GetMotorIsRunning(i) ? "Running ..." : "Not Running");
                }
            }
            labelMotorStatus.Text = motorLabel;

            // set the lights
            LightShow.SetDriveLights(TheRobot.MotorSpeed(1), TheRobot.MotorSpeed(2));
        }

      

        //  User Interface on Robot Tab
        private void checkBoxMonitorMotors_CheckedChanged(object sender, EventArgs e)
        {
            TheRobot.MonitorMotorSpeed = checkBoxMonitorMotorsSpeed.Checked;
            MotorsStatusUpdateUi();
        }

        private void checkBoxMonitorMotorsTach_CheckedChanged(object sender, EventArgs e)
        {
            TheRobot.MonitorMotorTach = checkBoxMonitorMotorsTach.Checked;
            MotorsStatusUpdateUi();
        }


        /// <summary>
        /// Keyboard Input Handling 
        /// pass keyboard key presses along to the robot
        /// </summary>
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (motorsTab.ControlMotors)
            {
                TheRobot.KeyboardDrivingInput(e.KeyValue);
            }
        }


        //  Joystick
        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            comboBoxJoystickPaths.Items.Clear();

            List<string> paths = Joystick.GetJoystickPaths();
            foreach (var nextPath in paths)
                comboBoxJoystickPaths.Items.Add(nextPath);
            if (paths.Count == 0)
                comboBoxJoystickPaths.Items.Add(NoneFound);
        }


        /// <summary>
        /// Check Use Joystick handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConnectJoystick_Click(object sender, EventArgs e)
        {
            string selectedPath = "";
            if (!Joystick.IsConnected && PlatformHelper.PlatformHelper.RunningPlatform() == Platform.Linux)
            {
                selectedPath = (string)comboBoxJoystickPaths.SelectedItem;
                if (selectedPath == null || selectedPath.Length == 0 || selectedPath == NoneFound)
                {
                    MessageBox.Show("Please select a path for the joystick!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            buttonConnectJoystick.Enabled = false;
            backgroundWorkerConnectJoystick.RunWorkerAsync(selectedPath);
        }
        //
        //  Background worker for connect to joystick
        private void backgroundWorkerConnectJoystick_DoWork(object sender, DoWorkEventArgs e)
        {
            if (Joystick.IsConnected)
            {
                e.Result = true;
                Joystick.Disconnect();
            }
            else
            {
               
                    e.Result = Joystick.ConnectToJoystick(JoystickType.XBox, "/dev/input/" + e.Argument);     //  TODO - from UI on this page
               
            }
        }
        //
        private void backgroundWorkerConnectJoystick_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((bool)e.Result != true)
            {
                MessageBox.Show("Failed to connect to Joystick !", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            buttonConnectJoystick.Enabled = true;

            if (Joystick.IsConnected)
            {
               
                    buttonConnectJoystick.Visible = true;

                buttonConnectJoystick.Text = "Disconnect";
                comboBoxJoystickPaths.Visible = false;
                buttonRefresh.Visible = false;
                motorsTab.SetJoystickModeDisplay();
            }
            else
            {
                comboBoxJoystickPaths.Visible = true;
                buttonRefresh.Visible = true;
                buttonConnectJoystick.Text = "Connect";
                motorsTab.SetJoystickDisconnectedDisplay();
            }
        }

        //   todo - joystick disconnect event handler

      


        /// <summary>
        /// XBox Controller event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Joystick_XboxJoystickEventHandler(object sender, SimpleJoy.XBoxJoystickEventArgs e)
        {
            JoystickUserInterfaceControls(e);

            if (motorsTab.ControlMotors)
                TheRobot.XboxDrigingInput(e);
        }



        // Joystick control of UI
        //
        long timeSinceTabSwitch = 0;
        long timeSinceDpadSwitch = 0;
        long timeSinceHomeBtn = 0;

        /// <summary>
        /// Joystick UI Control
        /// </summary>
        void JoystickUserInterfaceControls(SimpleJoy.XBoxJoystickEventArgs e)
        {
            long tickCount = Environment.TickCount;

            //  Back and Start Button cycle tab pages
            if ((e.Data.StartBtn || e.Data.BackBtn) && (tickCount - timeSinceTabSwitch > 200))
            {
                timeSinceTabSwitch = tickCount;

                int newIndex = tabControlRobot.SelectedIndex + (e.Data.StartBtn ? 1 : -1);
                if (newIndex == tabControlRobot.TabPages.Count)
                    newIndex = 0;
                else if (newIndex == -1)
                    newIndex = tabControlRobot.TabPages.Count - 1;

                this.BeginInvoke(new MethodInvoker(delegate()
                {
                    tabControlRobot.SelectedIndex = newIndex;
                }));
            }

            //  DPad selects from program tab (when program tab is visible)
            if (e.Data.Dpad && (tickCount - timeSinceDpadSwitch > 200))
            {
                timeSinceDpadSwitch = tickCount;

                if (tabControlRobot.SelectedTab == tabPageFiles && filesTab.ListViewFiles.Items.Count > 0)
                {
                    if (TheRobot.IsConnected && (e.Data.DpadRight))
                    {
                        var selected = filesTab.ListViewFiles.SelectedItems;
                        if (selected.Count > 0)
                        {
                            //  execute the file selected
                            TheRobot.RunProgram((BrickFile)selected[0].Tag);
                        }
                    }
                    else if (e.Data.DpadUp || e.Data.DpadDown)
                    {
                        int oldSelection = 0;
                        int increment = 0;
                        if (filesTab.ListViewFiles.SelectedItems.Count > 0)
                        {
                            oldSelection = filesTab.ListViewFiles.SelectedIndices[0];
                            increment = (e.Data.DpadUp ? -1 : 1);
                        }
                        int newSelection = oldSelection + increment;
                        if (newSelection == filesTab.ListViewFiles.Items.Count)
                            newSelection = 0;
                        else if (newSelection == -1)
                            newSelection = (filesTab.ListViewFiles.Items.Count - 1);

                        this.BeginInvoke(new MethodInvoker(delegate()
                        {
                            filesTab.ListViewFiles.Select();
                            filesTab.ListViewFiles.SelectedItems.Clear();
                            filesTab.ListViewFiles.Items[newSelection].Selected = true;

                        }));

                    }
                }
            }

            //  home button changes driving mode
            if (e.Data.HomeBtn && (tickCount - timeSinceHomeBtn > 1000))
            {
                timeSinceHomeBtn = tickCount;

                MonoRobot.XboxJoystickMode newMode = TheRobot.XBoxJoystickUsageMode + 1;
                if (newMode == XboxJoystickMode.outOfRange)
                    newMode = (MonoRobot.XboxJoystickMode)0;

                TheRobot.XBoxJoystickUsageMode = newMode;

                this.BeginInvoke(new MethodInvoker(delegate()
                {
                    motorsTab.SetJoystickModeDisplay();

                }));
            }
        }


        private void checkBoxLightSwitchCore_CheckedChanged(object sender, EventArgs e)
        {
            LightShow.EnableHeartbeat(!LightShow.HeartbeatOn);
        }

        private void checkBoxLightSwitchWelder_CheckedChanged(object sender, EventArgs e)
        {
            LightShow.WelderOn = !LightShow.WelderOn;
        }

        private void checkBoxLightSwitchCtrlTwr_CheckedChanged(object sender, EventArgs e)
        {
            int state = LightShow.LightCmdCenterMain.DigitalRead();

            if (checkBoxLightSwitchCtrlTwr.Checked && state != 1)
            {
                LightShow.LightCmdCenterMain.Write(1);
            }
            else if (!checkBoxLightSwitchCtrlTwr.Checked && state != 0)
            {
                LightShow.LightCmdCenterMain.Write(0);
            }
        }

        private void checkBoxLightSwitchPwrMain_CheckedChanged(object sender, EventArgs e)
        {
            int state = LightShow.LightPwrMain.DigitalRead();

            if (checkBoxLightSwitchPwrMain.Checked && state != 1)
            {
                LightShow.LightPwrMain.Write(1);
            }
            else if (!checkBoxLightSwitchPwrMain.Checked && state != 0)
            {
                LightShow.LightPwrMain.Write(0);
            }
        }

        private void checkBoxLightSwitchPwr1_CheckedChanged(object sender, EventArgs e)
        {
            int state = LightShow.LightPwrLower1.DigitalRead();

            if (checkBoxLightSwitchPwr1.Checked && state != 1)
            {
                LightShow.LightPwrLower1.Write(1);
            }
            else if (!checkBoxLightSwitchPwr1.Checked && state != 0)
            {
                LightShow.LightPwrLower1.Write(0);
            }
        }

        private void checkBoxLightSwitchPwr2_CheckedChanged(object sender, EventArgs e)
        {
            int state = LightShow.LightPwrLower2.DigitalRead();

            if (checkBoxLightSwitchPwr2.Checked && state != 1)
            {
                LightShow.LightPwrLower2.Write(1);
            }
            else if (!checkBoxLightSwitchPwr2.Checked && state != 0)
            {
                LightShow.LightPwrLower2.Write(0);
            }
        }
    }
}
