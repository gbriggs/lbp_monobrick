﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using MonoRobot;


namespace ev3CPanel
{
    public partial class ConnectionForm : Form
    {
        public ConnectionForm()
        {
            InitializeComponent();

            //  TODO init combo box selection from prefs
            radioBluetooth.Checked = true;
            if (PlatformHelper.PlatformHelper.RunningPlatform() == PlatformHelper.Platform.Linux)
            {
                comboBoxAddress.Items.Clear();
                comboBoxAddress.Items.Add("rfcomm0");
                comboBoxAddress.Text = "rfcomm0";
            }
            else
            {
                comboBoxAddress.Text = "COM3";
            }
        }

        public MonoRobot.ConnectMethod ConnectMethod
        {
            get
            {
                if (radioBluetooth.Checked)
                    return MonoRobot.ConnectMethod.Bluetooth;
                else if (radioWiFi.Checked)
                    return MonoRobot.ConnectMethod.WiFi;
                else
                    return MonoRobot.ConnectMethod.Usb;
            }
        }

        public string Address
        {
            get
            {
                if (comboBoxAddress.SelectedItem != null)
                    return comboBoxAddress.SelectedItem.ToString();
                else
                    return "";
            }
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        
        }

        private void radioUsb_CheckedChanged(object sender, EventArgs e)
        {
            SetConnectionUi();
        }

        private void radioBluetooth_CheckedChanged(object sender, EventArgs e)
        {
            SetConnectionUi();
        }

        private void radioWiFi_CheckedChanged(object sender, EventArgs e)
        {
            SetConnectionUi();
        }

        void SetConnectionUi()
        {
            bool showAddress = false;
            string addressLabel = "";
         
            switch ( ConnectMethod )
            {
                case MonoRobot.ConnectMethod.Usb:
                    break;

                case MonoRobot.ConnectMethod.Bluetooth:
                    addressLabel = "COM Port";
                    showAddress = true;
                    break;

                case MonoRobot.ConnectMethod.WiFi:
                    break;
            }

            labelAddress.Visible = showAddress;
            labelAddress.Text = addressLabel;
            comboBoxAddress.Visible = showAddress;
          

        }

    }
}
