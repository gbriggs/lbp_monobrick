﻿namespace ev3CPanel
{
    partial class ConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioUsb = new System.Windows.Forms.RadioButton();
            this.radioBluetooth = new System.Windows.Forms.RadioButton();
            this.radioWiFi = new System.Windows.Forms.RadioButton();
            this.labelAddress = new System.Windows.Forms.Label();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxAddress = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioUsb
            // 
            this.radioUsb.AutoSize = true;
            this.radioUsb.Location = new System.Drawing.Point(18, 26);
            this.radioUsb.Name = "radioUsb";
            this.radioUsb.Size = new System.Drawing.Size(47, 17);
            this.radioUsb.TabIndex = 0;
            this.radioUsb.Text = "USB";
            this.radioUsb.UseVisualStyleBackColor = true;
            this.radioUsb.CheckedChanged += new System.EventHandler(this.radioUsb_CheckedChanged);
            // 
            // radioBluetooth
            // 
            this.radioBluetooth.AutoSize = true;
            this.radioBluetooth.Location = new System.Drawing.Point(18, 50);
            this.radioBluetooth.Name = "radioBluetooth";
            this.radioBluetooth.Size = new System.Drawing.Size(70, 17);
            this.radioBluetooth.TabIndex = 1;
            this.radioBluetooth.TabStop = true;
            this.radioBluetooth.Text = "Bluetooth";
            this.radioBluetooth.UseVisualStyleBackColor = true;
            this.radioBluetooth.CheckedChanged += new System.EventHandler(this.radioBluetooth_CheckedChanged);
            // 
            // radioWiFi
            // 
            this.radioWiFi.AutoSize = true;
            this.radioWiFi.Location = new System.Drawing.Point(18, 73);
            this.radioWiFi.Name = "radioWiFi";
            this.radioWiFi.Size = new System.Drawing.Size(49, 17);
            this.radioWiFi.TabIndex = 2;
            this.radioWiFi.TabStop = true;
            this.radioWiFi.Text = "Wi-Fi";
            this.radioWiFi.UseVisualStyleBackColor = true;
            this.radioWiFi.CheckedChanged += new System.EventHandler(this.radioWiFi_CheckedChanged);
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Location = new System.Drawing.Point(32, 132);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(45, 13);
            this.labelAddress.TabIndex = 6;
            this.labelAddress.Text = "Address";
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(96, 216);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(75, 23);
            this.buttonConnect.TabIndex = 8;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioUsb);
            this.groupBox1.Controls.Add(this.radioBluetooth);
            this.groupBox1.Controls.Add(this.radioWiFi);
            this.groupBox1.Location = new System.Drawing.Point(26, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(207, 105);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connect Using";
            // 
            // comboBoxAddress
            // 
            this.comboBoxAddress.FormattingEnabled = true;
            this.comboBoxAddress.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9"});
            this.comboBoxAddress.Location = new System.Drawing.Point(83, 129);
            this.comboBoxAddress.Name = "comboBoxAddress";
            this.comboBoxAddress.Size = new System.Drawing.Size(150, 21);
            this.comboBoxAddress.TabIndex = 10;
            // 
            // ConnectionForm
            // 
           // this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          //  this.AutoScaleMode =  System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.comboBoxAddress);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.labelAddress);
            this.Name = "ConnectionForm";
            this.Text = "ConnectionForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioUsb;
        private System.Windows.Forms.RadioButton radioBluetooth;
        private System.Windows.Forms.RadioButton radioWiFi;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxAddress;
    }
}