﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MonoRobot;

namespace ev3CPanel
{
    public partial class SensorControl : UserControl
    {
        protected int SensorIndex { get; set; }
        protected Robot Robot { get; set; }

        public SensorControl()
        {
            InitializeComponent();
        }

        public void InitControl(Robot robot, int index, string label)
        {
            groupBoxSensor.Text = label;
            SensorIndex = index;
            Robot = robot;
        }

        public void ReInitControl()
        {
            //  get the sensor for this control
            labelName.Text = Robot.SensorName(SensorIndex);
            labelMode.Text = Robot.SensorModeName(SensorIndex);

            FillModeComboBox();

            //  set combo box settings
            comboBoxMode.SelectedItem = Robot.SensorMode(SensorIndex);
        }

        public void UpdateLabels()
        {
            labelReading.Text = Robot.SensorReading(SensorIndex);
        }

        public void SetComboBox(string value)
        {
            if (!ChoosingSensor)
            {
                foreach (var nextItem in comboBoxMode.Items)
                {
                    if (nextItem.ToString().CompareTo(value) == 0)
                    {
                        comboBoxMode.SelectedItem = nextItem;
                        break;
                    }
                }
            }
        }
        public void UpdateControl()
        {
            if ( Robot.IsConnected )
            {
                string modeName = Robot.SensorModeName(SensorIndex);

                SetComboBox(modeName);
            }

            UpdateLabels();
        }


        public void EnableControl(bool enable)
        {
            comboBoxMode.Enabled = enable;
        }

        protected void FillModeComboBox()
        {
            comboBoxMode.Items.Clear();

            switch ( Robot.SensorType(SensorIndex) )
            {
                case MonoBrick.EV3.SensorType.Touch:
                    comboBoxMode.Items.Add(MonoBrick.EV3.TouchMode.Boolean);
                    comboBoxMode.Items.Add(MonoBrick.EV3.TouchMode.Count);
                    break;

                case MonoBrick.EV3.SensorType.IR:
                    comboBoxMode.Items.Add(MonoBrick.EV3.IRMode.Proximity);
                    comboBoxMode.Items.Add(MonoBrick.EV3.IRMode.Remote);
                    comboBoxMode.Items.Add(MonoBrick.EV3.IRMode.Seek);
                    break;

                case MonoBrick.EV3.SensorType.Color:
                    comboBoxMode.Items.Add(MonoBrick.EV3.ColorMode.Ambient);
                    comboBoxMode.Items.Add(MonoBrick.EV3.ColorMode.Color);
                    comboBoxMode.Items.Add(MonoBrick.EV3.ColorMode.Raw);
                    comboBoxMode.Items.Add(MonoBrick.EV3.ColorMode.Reflection);
                    break;

                default:
                    //  unknown or unhandled sensor type
                    break;
            }
        }

        private void comboBoxMode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxMode.SelectedItem != null)
            {
                ComboBox cb = (ComboBox)sender;

                Robot.SetSensorMode(SensorIndex, (MonoBrick.EV3.SensorMode)comboBoxMode.Items[cb.SelectedIndex]);
            }
        }

        bool ChoosingSensor = false;

        private void comboBoxMode_DropDown(object sender, EventArgs e)
        {
            ChoosingSensor = true;
        }

        private void comboBoxMode_DrawItem(object sender, DrawItemEventArgs e)
        {
            ChoosingSensor = false;
        }
    }
}
