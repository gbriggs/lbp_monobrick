﻿MonoBrick

The MonoBrick communication library is a LEGO Mindstorms 
communication library written in C# that works with the 
standard firmware on both the EV3 and NXT.

From:  http://www.monobrick.dk/software/monobrick/

