﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    public enum LogLevel
    {
        DEBUG,
        INFO,
        WARN,
        ERROR,
    }

    static class Logging
    {
        public static LogLevel LevelToLog = LogLevel.DEBUG;

        public static void LogFunction(object sender, object function, LogLevel level, object data)
        {
            if (level < LevelToLog)
                return;

            var timeString = DateTime.Now.ToString("HH-mm-ss.fff");
            var logString = $"{timeString} {level,5} {sender}.{function} {data}";
            Console.WriteLine(logString);
        }
    }
}
