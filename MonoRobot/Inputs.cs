﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    public enum XboxJoystickMode
    {
        TrackSteer,
        RightStick,
        outOfRange
    };

    //  Quadrant Enum
    //
    enum quadrant
    {
        NE,
        NW,
        SW,
        SE
    }

    //  Track driving direction
    //
    enum TrackDrivingDirection
    {
        Stop,
        Forward,
        Back
    }

    //  Joystick Direction
    enum JoystickDirection
    {
        Stop,
        Forward,
        ForwardRight,
        SpinRight,
        ReverseRight,
        Reverse,
        ReverseLeft,
        SpinLeft,
        ForwardLeft
    }

   
    
    
}
