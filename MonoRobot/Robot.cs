﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoBrick.EV3;
using System.Threading;
using System.Timers;
using SimpleJoy;
using System.Xml;
using static MonoRobot.Logging;

namespace MonoRobot
{
    // MonoRobot
    // by http://LittleBytesOfPi.com
    // This class is a wrapper around the MonoBrick Library from http://MonoBrick.dk
    // to control an EV3 robot for our project http://www.littlebytesofpi.com/ev3/


    /// <summary>
    /// Robot class
    /// interface point between client application and the brick library
    /// </summary>
    public partial class Robot
    {
        // Public Interfaces
        //
        #region PublicInterfaces

        //  Robot Events
        //
        public event ConnectionEventHandler ConnectionStateChanged;
        public event MotorEventHandler MotorStateChanged;
        public event SensorEventHandler SensorStateChanged;

        //  Monitor
        public bool MonitorMotorSpeed { get; set; }
        public bool MonitorMotorTach { get; set; }
        public bool MonitoringMotors { get { return MonitorMotorSpeed || MonitorMotorTach; } }

        //  Sensors
        public bool MonitorSensors { get { return MonitorColorSensor || MonitorIrSensor || MonitorTouchSensor; } }
        public bool MonitorColorSensor { get; set; }
        public bool MonitorIrSensor { get; set; }
        public bool MonitorTouchSensor { get; set; }


        //  Motors
        //  Access brick motors by index
        //  index 0 - 3 for first brick in chain, 4-7 for second ...
        //
        public int MotorCount { get { return Motors.Count; } }


        /// <summary>
        /// Get the latest speed of the motor
        /// this information is the latest from the status query loop 
        /// </summary>
        public int MotorSpeed(int motorIndex)
        {
            if (Motors.Count > 0 && Motors.Count > motorIndex)
            {
                return Motors[motorIndex].Speed;
            }
            return 0;
        }


        /// <summary>
        /// Update and get the current speed of the motor
        /// this will query the brick and return the current result 
        /// </summary>
        public int GetMotorSpeed(int motorIndex)
        {
            if (Motors.Count > 0 && Motors.Count > motorIndex)
            {
                try
                {
                    return Motors[motorIndex].GetSpeed();
                }
                catch (Exception e)
                {
                    LogFunction(this, "GetMotorSpeed", LogLevel.ERROR, e);
                }
            }
            return 0;
        }


        /// <summary>
        /// Get the is running state flag
        /// this will query the brick for current information
        /// </summary>
        public bool GetMotorIsRunning(int motorIndex)
        {
            if (Motors.Count > 0 && Motors.Count > motorIndex)
            {
                return Motors[motorIndex].IsRunning();
            }
            return false;
        }


        /// <summary>
        /// Get the brick port the motor is connected to
        /// will be port A - D
        /// </summary>
        public string MotorPort(int motorIndex)
        {
            if (Motors.Count > 0 && Motors.Count > motorIndex)
            {
                return Motors[motorIndex].ConnectedPort;
            }
            return "not connected";
        }


        /// <summary>
        /// Get the tacho count for the motor 
        /// this information is the latest from the status loop
        /// </summary>
        public int MotorTachoCount(int motorIndex)
        {
            if (Motors.Count > 0 && Motors.Count > motorIndex)
            {
                return Motors[motorIndex].TachoCount;
            }
            return 0;
        }


        /// <summary>
        /// Update and get the current tacho count for the motor 
        /// this will query the brick and return current result
        /// </summary>
        public int GetMotorTachoCount(int motorIndex)
        {
            if (Motors.Count > 0 && Motors.Count > motorIndex)
            {
                try
                {
                    return Motors[motorIndex].GetTachoCount();
                }
                catch (Exception e)
                {
                    LogFunction(this, "GetMotorTachoCount", LogLevel.ERROR, e);
                }
            }
            return 0;
        }


        /// <summary>
        /// Set Motor Speed
        /// sets the motor to the speed speed
        /// </summary>
        /// <param name="speed">motor speed from -100 to 100</param>
        public void MotorSetSpeed(int motorIndex, int speed)
        {
            if (Brick == null || !IsConnected)
                return;

            ProcessSetMotor(motorIndex, speed);
        }


        /// <summary>
        /// Set Motor Turn
        /// turns the motor the specified number of degrees
        /// </summary>
        /// <param name="degrees"> +/-ve degrees to turn</param>
        public void MotorSetTurn(int motorIndex, int degrees, int speed)
        {
            if (Brick == null || !IsConnected)
                return;

            ProcessTurnMotor(motorIndex, degrees, speed);
        }


        /// <summary>
        /// Turn off the motor
        /// </summary>
        public void MotorOff(int motorIndex, bool resetTach = false)
        {
            if (Brick == null || !IsConnected)
                return;

            ProcessMotorOff(motorIndex, resetTach);
        }


        //  Sensors
        //  Access sensors by index
        //  index 0-3 for first brick in chain, 4-7 for second ...
        //
        public int SensorCount { get { return Sensors == null ? 0 : Sensors.Count; } }


        /// <summary>
        /// Get the name of the sensor
        /// </summary>
        public string SensorName(int sensorIndex)
        {
            if (Sensors.Count > 0 && Sensors.Count > sensorIndex)
            {
                return Sensors[sensorIndex].Name;
            }
            return "---";
        }


        /// <summary>
        /// Get the sensor mode
        /// </summary>
        public SensorMode SensorMode(int sensorIndex)
        {
            if (Sensors.Count > 0 && Sensors.Count > sensorIndex)
            {
                return Sensors[sensorIndex].SensorMode;
            }
            return MonoBrick.EV3.SensorMode.Mode0;
        }


        /// <summary>
        /// Get the sensor mode as a string
        /// </summary>
        public string SensorModeName(int sensorIndex)
        {
            if (Sensors.Count > 0 && Sensors.Count > sensorIndex)
            {
                Sensor sensor = Sensors[sensorIndex];
                switch (sensor.SensorType)
                {
                    case MonoBrick.EV3.SensorType.Color:
                        {
                            ColorSensor clrSensor = (ColorSensor)sensor;
                            return clrSensor.Mode.ToString();
                        }
                    case MonoBrick.EV3.SensorType.IR:
                        {
                            IRSensor irSensor = (IRSensor)sensor;
                            return irSensor.Mode.ToString();
                        }
                    case MonoBrick.EV3.SensorType.Touch:
                        {
                            TouchSensor touchSensor = (TouchSensor)sensor;
                            return touchSensor.Mode.ToString();
                        }
                    default:
                        break;
                }
            }
            return "---";
        }


        /// <summary>
        /// Get the sensor mode as a string
        /// </summary>
        /// <param name="index">index of the sensor</param>
        public string SensorModeName(Sensor sensor)
        {
            switch (sensor.SensorType)
            {
                case MonoBrick.EV3.SensorType.Color:
                    {
                        ColorSensor clrSensor = (ColorSensor)sensor;
                        return clrSensor.Mode.ToString();
                    }
                case MonoBrick.EV3.SensorType.IR:
                    {
                        IRSensor irSensor = (IRSensor)sensor;
                        return irSensor.Mode.ToString();
                    }
                case MonoBrick.EV3.SensorType.Touch:
                    {
                        TouchSensor touchSensor = (TouchSensor)sensor;
                        return touchSensor.Mode.ToString();
                    }
                default:
                    break;
            }

            return "---";
        }


        /// <summary>
        /// Get the sensor type
        /// </summary>
        public SensorType SensorType(int sensorIndex)
        {
            if (Sensors.Count > 0 && Sensors.Count > sensorIndex)
            {
                return Sensors[sensorIndex].SensorType;
            }
            return MonoBrick.EV3.SensorType.Unknown;
        }


        /// <summary>
        /// Get the reading from the sensor
        /// this is latest reading from the status loop
        /// </summary>
        public string SensorReading(int sensorIndex)
        {
            if (Sensors.Count > 0 && Sensors.Count > sensorIndex)
            {
                return Sensors[sensorIndex].LatestReading;
            }
            return "---";
        }


        /// <summary>
        /// Get the reading from the sensor
        /// this will query the brick and return the current reading
        /// </summary>
        public string GetSensorReading(int sensorIndex)
        {
            if (Sensors.Count > 0 && Sensors.Count > sensorIndex)
            {
                try
                {
                    return Sensors[sensorIndex].ReadAsString();
                }
                catch (Exception e)
                {
                    LogFunction(this, "GetSensorReading", LogLevel.ERROR, e);
                }
            }
            return "---";
        }


        /// <summary>
        /// Set the mode of the sensor
        /// </summary>
        /// <param name="index">index of the sensor</param>
        public void SetSensorMode(int sensorIndex, SensorMode mode)
        {
            if (Sensors.Count > 0 && Sensors.Count > sensorIndex)
            {
                Sensor sensor = Sensors[sensorIndex];
                SetSensorMode(sensor, mode);
            }
        }


        //  Controller Inputs
        /// <summary>
        /// Input XBox Controller data
        /// </summary>
        public void XboxDrigingInput(XBoxJoystickEventArgs data)
        {
            switch (XBoxJoystickUsageMode)
            {
                case XboxJoystickMode.TrackSteer:
                    ProcessXboxControllerInputTrackSteeringMode(data);
                    break;

                case XboxJoystickMode.RightStick:
                    ProcessXboxControllerInputRightStickMode(data);
                    break;
            }

        }


        /// <summary>
        /// Input Keyboard key
        /// </summary>
        public void KeyboardDrivingInput(int key)
        {
            if (Brick == null || !IsConnected)
                return;

            ProcessKeyboardDrivingInput(key);
        }


        //  Files
        /// <summary>
        /// Run Program
        /// </summary>
        /// <param name="file">file object to run</param>
        public void RunProgram(BrickFile file)
        {
            ProcessRunProgram(file);
        }

        /// <summary>
        /// Stop the running program
        /// TODO - does not work?
        /// </summary>
        public void StopRunningProgram()
        {
            ProcessStopRunningProgram();
        }


        //  Connection State

        public bool IsConnected { get { return ConnectionStatus == ConnectionState.Connected; } }

        public ConnectMethod ConnectionMethod { get; protected set; }

        /// <summary>
        /// Disconnect from the robot and stop the robot command queue thread
        /// does not turn off robot hardware.
        /// </summary>
        public void ShutDown()
        {
            DisconnectFromRobot();

            RunCommandQueueThread = false;
            if (CommandQueueThread.IsAlive)
            {
                //  send an interrupt
                CommandQueueThread.Interrupt();
                // wait for the thread run function to exit
                CommandQueueThread.Join();
            }
        }

        #endregion //  Public Interfaces


        //  Construction and Initialization
        //
        #region Construction

        /// <summary>
        /// Constructor
        /// </summary>
        public Robot()
        {
            InitializeRobot();

            //  default start monitor everything
            MonitorMotorSpeed = true;
            MonitorMotorTach = true;
            MonitorColorSensor = true;
            MonitorTouchSensor = true;
            MonitorIrSensor = true;

            //  Status Timer to request status updates
            StatusTimer = new System.Timers.Timer();
            StatusTimer.Elapsed += new ElapsedEventHandler(OnStatusTimer);
            StatusTimer.Interval = 250; //  4HZ timer
            StatusTimer.Enabled = true;

            //  Communication requests go through a queue serviced by this thread
            RunCommandQueueThread = true;
            CommandQueueThread = new Thread(new ThreadStart(CommandQueueThreadRun));
            CommandQueueThread.Start();

            //  Connection Status
            _ConnectionStatus = ConnectionState.Disconnected;

            //  List of motors and sensors
            Motors = new List<Motor>();
            Sensors = new List<Sensor>();

            XBoxJoystickUsageMode = XboxJoystickMode.TrackSteer;

            UpdateRobotFiles();
        }

        //  Cache Sensors on connection to robot for easier access


        public XboxJoystickMode XBoxJoystickUsageMode { get; set; }

        //  Status Timer
        //
        System.Timers.Timer StatusTimer;

        //  Brick Object
        Brick<Sensor, Sensor, Sensor, Sensor> Brick;
        private List<Motor> Motors { get; set; }
        private List<Sensor> Sensors { get; set; }
        //  cache exact sensor for easier access
        public ColorSensor RobotColorSensor { get; protected set; }
        public TouchSensor RobotTouchSensor { get; protected set; }
        public IRSensor RobotIrSensor { get; protected set; }

        //  Command Queue
        private static Object LockCommandQueue = new Object();
        private AutoResetEvent NotifyData = new AutoResetEvent(false);
        List<RobotCommand> CommandQueue = new List<RobotCommand>();
        //
        private Thread CommandQueueThread;
        public bool RunCommandQueueThread { get; protected set; }


        /// <summary>
        /// Initialize Function
        /// </summary>
        protected void InitializeRobot()
        {
            if (Brick != null && Brick.Connection.IsConnected)
                Brick.Connection.Close();

            Brick = null;

            //  Joystick handling
            LastJoystickDirection = JoystickDirection.Forward;
            LastJoystickSpeed = 0;
        }


        /// <summary>
        /// Set Sensor Mode
        /// </summary>
        protected void SetSensorMode(Sensor sensor, SensorMode mode)
        {
            switch (sensor.SensorType)
            {
                case MonoBrick.EV3.SensorType.Color:
                    {
                        ColorSensor clrSensor = (ColorSensor)sensor;

                        RobotCommand cmd = new RobotCommand(this, () =>
                        {
                            clrSensor.Mode = (ColorMode)mode;
                        });
                        QueueCommand(cmd);
                    }
                    break;

                case MonoBrick.EV3.SensorType.IR:
                    {
                        IRSensor irSensor = (IRSensor)sensor;
                        RobotCommand cmd = new RobotCommand(this, () =>
                        {
                            irSensor.Mode = (IRMode)mode;
                        });
                        QueueCommand(cmd);
                    }
                    break;

                case MonoBrick.EV3.SensorType.Touch:
                    {
                        TouchSensor touchSensor = (TouchSensor)sensor;
                        RobotCommand cmd = new RobotCommand(this, () =>
                        {
                            touchSensor.Mode = (TouchMode)mode;
                        });
                        QueueCommand(cmd);
                    }
                    break;
            }
        }

        #endregion


        //  Status Timer
        //
        #region StatusTimer

        /// <summary>
        /// Status Timer tick
        /// </summary>
        private void OnStatusTimer(object source, ElapsedEventArgs e)
        {
            try
            {

                //  check to see if we are connected
                if (Brick == null || !IsConnected)
                    return;

                //  check to see if we are monitoring anything
                if (!MonitoringMotors && !MonitorSensors)
                    return;

                //  fire off a status event
                RobotCommand cmd = new RobotCommand(Brick, () =>
                {
                    if (MonitorMotorSpeed)
                    {
                        foreach (var nextMotor in Motors)
                        {
                            if (nextMotor != null)
                                nextMotor.GetSpeed();
                        }
                    }
                    if (MonitorMotorTach)
                    {
                        foreach (var nextMotor in Motors)
                        {
                                if( nextMotor != null )
                                nextMotor.GetTachoCount();
                        }
                    }
                    if (MonitorSensors)
                    {
                        if (MonitorColorSensor && RobotColorSensor != null)
                            RobotColorSensor.ReadAsString();
                        if (MonitorIrSensor && RobotIrSensor != null)
                            RobotIrSensor.ReadAsString();
                        if (MonitorTouchSensor && RobotTouchSensor != null)
                            RobotTouchSensor.ReadAsString();
                    }

                    if (MonitoringMotors)
                    {
                        MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                    }

                    if (MonitorSensors)
                    {
                        SensorStateChanged?.Invoke(this, new SensorEventArgs());
                    }
                });
                QueueCommand(cmd);
            }
            catch (Exception ex)
            {
                LogFunction(this, "OnStatusTimer", LogLevel.ERROR, ex);
            }
        }

       
        #endregion


        //  Connection and Disconnection
        //
        #region Connection

        //  Connection State
        ConnectionState _ConnectionStatus;
        public ConnectionState ConnectionStatus
        {
            get
            {
                switch (_ConnectionStatus)
                {
                    case ConnectionState.Connected:
                        if (Brick == null || !Brick.Connection.IsConnected)
                            _ConnectionStatus = ConnectionState.Disconnected;
                        break;
                }
                return _ConnectionStatus;
            }
            protected set
            {
                _ConnectionStatus = value;
            }
        }



        /// <summary>
        /// Connection Helper Functions
        /// </summary>
        void StatusDisconnected()
        {
            _ConnectionStatus = ConnectionState.Disconnected;
            if (ConnectionStateChanged != null)
                ConnectionStateChanged(this, new ConnectionEventArgs(ConnectionState.Disconnected));
        }
        void StatusConnecting(string message)
        {
            _ConnectionStatus = ConnectionState.Connecting;
            if (ConnectionStateChanged != null)
                ConnectionStateChanged(this, new ConnectionEventArgs(ConnectionState.Connecting, message));
        }
        void StatusConnected()
        {
            _ConnectionStatus = ConnectionState.Connected;
            if (ConnectionStateChanged != null)
                ConnectionStateChanged(this, new ConnectionEventArgs(ConnectionState.Connected));
        }

        string ConnectionMethodString(string address)
        {
            switch (ConnectionMethod)
            {
                case ConnectMethod.Usb:
                    return "USB";
                case ConnectMethod.WiFi:
                    return "Wi-Fi";
                case ConnectMethod.Bluetooth:
                    return "Bluetooth on port " + address.ToString();
                default:
                    return "";
            }
        }

        /// <summary>
        /// Disconnect from the robot
        /// </summary>
        public bool DisconnectFromRobot()
        {
            try
            {
                if (Brick != null && Brick.Connection.IsConnected)
                {
                    _ConnectionStatus = ConnectionState.Connecting;
                    ClearQueue();
                    try
                    {
                        StatusDisconnected();
                        Brick.Connection.Close();
                        return true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Set Motor caught exception: " + e.ToString());
                        return false;
                    }
                    finally
                    {
                        InitializeRobot();

                    }
                }
            }
            catch (Exception e)
            {
                LogFunction(this, "DisconnectFromRobot", LogLevel.ERROR, e);
            }

            return false;
        }

        /// <summary>
        /// Connect to robot
        /// </summary>
        /// <param name="method">usb / bluetooth / wifi</param>
        /// <param name="address">com port for bt</param>
        public bool ConnectToRobot(ConnectMethod method, string address)
        {
            try
            {
                if (Brick != null && Brick.Connection.IsConnected)
                {
                    return false; //  bad state to call this
                }

                MakeConnectionToRobot(method, address);

                //  did we manage to connect
                if (Brick.Connection.IsConnected)
                {
                    SetupBrickMotors();

                    SetupBrickSensors();

                    //  File System
                    UpdateRobotFiles();

                    //  connected
                    StatusConnected();
                    return true;
                }
                else
                {
                    Brick = null;
                    ConnectionStateChanged?.Invoke(this, new ConnectionEventArgs(ConnectionState.Disconnected));
                }
            }
            catch (Exception e)
            {
                LogFunction(this, "ConnectToRobot", LogLevel.ERROR, e);
                StatusDisconnected();
            }

            return false;
        }

  
        /// <summary>
        /// Make connection to the robot using the specified connection type
        /// </summary>
        private void MakeConnectionToRobot(ConnectMethod method, string address)
        {
            try
            {
                ClearQueue();
                Motors = new List<Motor>();
                Sensors = new List<Sensor>();
                Brick = null;

                //  make the com port address proper for the platform
                var connectAddress = address;
                if (PlatformHelper.PlatformHelper.RunningPlatform() == PlatformHelper.Platform.Linux)
                    connectAddress = "/dev/" + address;

                LogFunction(this, "MakeConnectionToRobot", LogLevel.INFO, $"Connecting to {ConnectionMethodString(connectAddress)}.");

                //  Start connection
                _ConnectionStatus = ConnectionState.Connecting;
                ConnectionMethod = method;
                //  update the state event
                StatusConnecting($"Connecting to {ConnectionMethodString(connectAddress)}.");

                int maxRetries = 2;
                switch (method)
                {
                    case ConnectMethod.Usb: //  usb
                        Brick = new Brick<Sensor, Sensor, Sensor, Sensor>("usb");
                        break;

                    case ConnectMethod.Bluetooth: //  bluetooth
                        Brick = new Brick<Sensor, Sensor, Sensor, Sensor>(connectAddress);
                        maxRetries = 5;
                        break;

                    case ConnectMethod.WiFi: //  wi fi
                        Brick = new Brick<Sensor, Sensor, Sensor, Sensor>("wifi");
                        break;
                }

                //  attempt to connect
                int connectionRetries = 0;
                while (!Brick.Connection.IsConnected)
                {
                    try
                    {
                        Brick.Connection.Open();
                    }
                    catch (Exception exception)
                    {
                        LogFunction(this, "MakeConnectionToRobot", LogLevel.ERROR, $"Failed to connect: {exception}");
                        Thread.Sleep(1000);

                        connectionRetries++;
                        if (connectionRetries > maxRetries)
                        {
                            //  failed to connect
                            StatusDisconnected();
                            return;
                        }

                        //  try again
                        StatusConnecting(string.Format("Connecting to {0}. Retry: {1}", ConnectionMethodString(connectAddress), connectionRetries.ToString()));

                        Brick.Connection.Close();
                        Thread.Sleep(250);
                    }
                }
            }
            catch (Exception e)
            {
                LogFunction(this, "MakeConnectionToRobot", LogLevel.ERROR, e);
            }
        }


        /// <summary>
        /// Setup the brick motors after connection
        /// </summary>
        private void SetupBrickMotors()
        {
            //  setup the brick vehicle configuration
            Brick.Vehicle.LeftPort = MonoBrick.EV3.MotorPort.OutB;
            Brick.Vehicle.RightPort = MonoBrick.EV3.MotorPort.OutC;
            Brick.Vehicle.ReverseLeft = false;
            Brick.Vehicle.ReverseRight = false;
            RobotLeftDriveMotor = Brick.MotorB;
            RobotRightDriveMotor = Brick.MotorC;

            //  setup motors collection
            Motors.Add(Brick.MotorA);
            Motors.Add(Brick.MotorB);
            Motors.Add(Brick.MotorC);
            Motors.Add(Brick.MotorD);

            //  init motor state
            foreach (var nextMotor in Motors)
            {
                nextMotor.ResetTacho();
            }
        }


        /// <summary>
        /// Setup brick sensors after connection
        /// </summary>
        private void SetupBrickSensors()
        {
            try
            {
                //  setup sensors collection
                Sensor newSensor = null;
                string key = SensorHelper.TypeToKey(Brick.Sensor1.GetSensorType());
                if (SensorHelper.SensorDictionary.TryGetValue(key, out newSensor))
                {
                    Brick.Sensor1 = newSensor;
                    Brick.Sensor1.GetTypeAndMode();
                    if (Brick.Sensor1.SensorType != MonoBrick.EV3.SensorType.None)
                        Sensors.Add(Brick.Sensor1);
                }
                key = SensorHelper.TypeToKey(Brick.Sensor2.GetSensorType());
                if (SensorHelper.SensorDictionary.TryGetValue(key, out newSensor))
                {
                    Brick.Sensor2 = newSensor;
                    Brick.Sensor2.GetTypeAndMode();
                    if (Brick.Sensor2.SensorType != MonoBrick.EV3.SensorType.None)
                        Sensors.Add(Brick.Sensor2);
                }
                key = SensorHelper.TypeToKey(Brick.Sensor3.GetSensorType());
                if (SensorHelper.SensorDictionary.TryGetValue(key, out newSensor))
                {
                    Brick.Sensor3 = newSensor;
                    Brick.Sensor3.GetTypeAndMode();
                    if (Brick.Sensor3.SensorType != MonoBrick.EV3.SensorType.None)
                        Sensors.Add(Brick.Sensor3);
                }
                key = SensorHelper.TypeToKey(Brick.Sensor4.GetSensorType());
                if (SensorHelper.SensorDictionary.TryGetValue(key, out newSensor))
                {
                    Brick.Sensor4 = newSensor;
                    Brick.Sensor4.GetTypeAndMode();
                    if (Brick.Sensor4.SensorType != MonoBrick.EV3.SensorType.None)
                        Sensors.Add(Brick.Sensor4);
                }

                //  init sensors
                foreach (var nextSensor in Sensors)
                {
                    nextSensor.GetTypeAndMode();
                    nextSensor.GetName();
                    nextSensor.GetSymbole();

                    switch (nextSensor.SensorType)
                    {
                        case MonoBrick.EV3.SensorType.IR:
                            {
                                RobotIrSensor = (IRSensor)nextSensor;
                                string check = RobotIrSensor.ModeName;
                                RobotIrSensor.Mode = IRMode.Proximity;
                            }
                            break;

                        case MonoBrick.EV3.SensorType.Touch:
                            RobotTouchSensor = (TouchSensor)nextSensor;
                            RobotTouchSensor.Mode = TouchMode.Count;
                            break;

                        case MonoBrick.EV3.SensorType.Color:
                            {
                                RobotColorSensor = (ColorSensor)nextSensor;
                                RobotColorSensor.Mode = ColorMode.Color;
                            }

                            break;
                    }
                }
            }
            catch (Exception e)
            {
                LogFunction(this, "SetupBrickSensors", LogLevel.ERROR, e);
            }
        }

        #endregion  //  Connection


        // Brick File System
        //
        #region BrickFiles

        //  Brick Files Collection
        List<BrickFile> _BrickFiles = new List<BrickFile>();

        /// <summary>
        /// Get a copy of the BrickFiles collection
        /// </summary>
        public List<BrickFile> BrickFiles
        {
            get
            {
                List<BrickFile> files = new List<BrickFile>();
                files.AddRange(_BrickFiles);
                return files;
            }
        }

        /// <summary>
        /// Update Files from the default program path
        /// </summary>
        protected void UpdateRobotFiles()
        {
            _BrickFiles.Clear();

            /*  TODO - getting files from robot does not work?
           UpdateRobotFiles("/home/root/lms2012/prjs");*/

            //  Load files from XML doc
            XmlDocument fileList = new XmlDocument();
            try
            {
#if RPI
                fileList.Load("/home/pi/ev3/BrickPrograms.xml");
#else
                fileList.Load("./BrickPrograms.xml");
#endif

                XmlElement root = fileList.DocumentElement;

                var programFoldersNode = root.SelectSingleNode("ProgramFolders");
                if (programFoldersNode == null)
                    return;

                var programFolderNodes = programFoldersNode.SelectNodes("ProgramFolder");
                foreach (XmlNode nextProgramFolder in programFolderNodes)
                {
                    //  get the name of this program folder
                    XmlNode nameNode = nextProgramFolder.SelectSingleNode("Name");
                    if (nameNode == null)
                        continue;

                    string folderName = nameNode.InnerText;

                    var programs = nextProgramFolder.SelectNodes("File");
                    foreach (XmlNode nextProgram in programs)
                    {

                        string programName = nextProgram.InnerText;

                        //  Make a brick file object
                        BrickFile nextFile = new BrickFile(programName + ".rbf", 1024, "/home/root/lms2012/prjs/" + folderName);
                        _BrickFiles.Add(nextFile);
                    }
                }
            }
            catch (Exception e)
            {
                LogFunction(this, "UpdateRobotFiles", LogLevel.ERROR, e);
                _BrickFiles.Clear();
            }

           
        }

        /// <summary>
        /// Update Files
        /// todo - this is not working entirely correct?
        /// </summary>
        /// <param name="path">path to update from</param>
        protected void UpdateRobotFiles(string path)
        {

            var folderStructure = Brick.FileSystem.GetFolderStructure("/home/root/lms2012/prjs/");
            var fileQuery = from structure in folderStructure.RunThroughFolders()
                            from files in structure.FileList
                                //where (files.FileType == MonoBrick.FileType.Program)
                            select files;
            foreach (var file in fileQuery)
            {
                _BrickFiles.Add(file);

                Console.WriteLine(file.Name + " in " + file.Path);
            }


            //Brick.FileSystem.GetFolderInfo(path, out files, out subfolders);

            //Console.WriteLine("Files:");
            //foreach (var file in files)
            //{
            //    Console.WriteLine(file.Name);
            //    if (BrickFiles.Find(x => x.FullName == file.FullName) == null)
            //        _BrickFiles.Add(file);
            //}




            //Console.WriteLine(Environment.NewLine + "Subfolders:");
            //foreach (var folder in subfolders)
            //{
            //    Console.WriteLine(folder);
            //   UpdateRobotFiles(path + folder);
            //}

        }


        /// <summary>
        /// Run a program on the brick
        /// </summary>
        /// <param name="file">brick file to run</param>
        public void ProcessRunProgram(BrickFile file)
        {
            RobotCommand cmd = new RobotCommand(this, () =>
            {
                //  stop any running program
                Brick.StopProgram();
                //  start the requested program
                Brick.StartProgram(file);
            });
            QueueCommand(cmd);
        }


        /// <summary>
        /// Stop running the current program on the brick
        /// todo - does not appear to work?
        /// </summary>
        public void ProcessStopRunningProgram()
        {
            RobotCommand cmd = new RobotCommand(this, () =>
            {
                Brick.StopProgram();
            });
            QueueCommand(cmd);
        }

        #endregion //  BrickFiles


        //  Command Queue
        //
        #region CommandThread


        /// <summary>
        /// Queue up a command to send to the robot
        /// </summary>
        void QueueCommand(RobotCommand command)
        {
            lock (LockCommandQueue)
            {
                CommandQueue.Insert(0, command);

                NotifyData.Set();
            }
        }

        /// <summary>
        /// Clear the command queue
        /// </summary>
        void ClearQueue()
        {
            lock (LockCommandQueue)
            {
                CommandQueue.Clear();
            }
        }


        /// <summary>
        /// Command queue thread run function
        /// </summary>
        public void CommandQueueThreadRun()
        {
            while (RunCommandQueueThread)
            {
                try
                {
                    //  is the command queue empty
                    if (CommandQueue.Count == 0)
                    {
                        NotifyData.WaitOne();
                    }

                    //  empty the command queue
                    List<RobotCommand> queue;
                    lock (LockCommandQueue)
                    {
                        queue = new List<RobotCommand>(CommandQueue);
                        CommandQueue.Clear();
                    }

                    //  process the command queue (remote redundant commands)
                    List<RobotCommand> filteredQueue = ProcessCommandQueue(queue);

                    foreach (var nextCommand in filteredQueue)
                    {
                        try
                        {
                            //  verify connection to the brick before sending next command
                            if (Brick.Connection.IsConnected)
                                nextCommand.Command.Invoke();
                        }
                        catch (Exception e)
                        {
                            LogFunction(this, "CommandQueueThreadRun", LogLevel.ERROR, e);
                        }
                    }
                }
                catch (Exception e)
                {
                    //  Catch thread run interrupt exception
                    if (RunCommandQueueThread)
                        LogFunction(this, "CommandQueueThreadRun", LogLevel.ERROR, e);
                    else
                        return;

                }
            }
        }

        #endregion  //  Status Thread



    }
}
