﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    /// <summary>
    /// Helper functions to map keyboard letters to numpad
    //    qwe  789
    //    asd  456
    //    zxc  123
    //     vb   0.
    /// </summary>
    class KeyboardNumpadKeymap
    {
        static public bool IsD2Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D2 || key == ConsoleKey.NumPad2 || key == ConsoleKey.X);
        }
        static public bool IsD4Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D4 || key == ConsoleKey.NumPad4 || key == ConsoleKey.A);
        }

        static public bool IsD6Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D6 || key == ConsoleKey.NumPad6 || key == ConsoleKey.D);
        }

        static public bool IsD7Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D7 || key == ConsoleKey.NumPad7 || key == ConsoleKey.Q);
        }

        static public bool IsD8Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D8 || key == ConsoleKey.NumPad8 || key == ConsoleKey.W);
        }

        static public bool IsD9Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D9 || key == ConsoleKey.NumPad9 || key == ConsoleKey.E);
        }

        static public bool IsD0Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D0 || key == ConsoleKey.NumPad0 || key == ConsoleKey.V);
        }
    }
}
