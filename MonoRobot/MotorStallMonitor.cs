﻿using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    /// <summary>
    /// Object to monitor motor tach, and turn off any stalled motor
    /// </summary>
    class MotorStallMonitor
    {
        public MotorStallMonitor(Motor motor)
        {
            MotorMonitored = motor;
            turnedOffStalledMotor = true;
            tachoHistory = new Queue<int>();
        }

        Motor MotorMonitored { get; set; }

        bool turnedOffStalledMotor;

        int maxQueue = 5;
        Queue<int> tachoHistory;


        public void CheckForMotorStall()
        {
            tachoHistory.Enqueue(MotorMonitored.TachoCount);
            if ( tachoHistory.Count > maxQueue )
            {
                tachoHistory.Dequeue();
            }

            if (tachoHistory.Count >= maxQueue)
            {
                for (int i = 1; i < tachoHistory.Count; i++)
                {
                    if (tachoHistory.ElementAt(i - 1) != tachoHistory.ElementAt(i))
                    {
                        System.Diagnostics.Debug.WriteLine($"Reset stalled motor flag for {MotorMonitored.ConnectedPort}.");
                        turnedOffStalledMotor = false;
                        return;
                    }
                }
            }

            if (!turnedOffStalledMotor)
            {
                System.Diagnostics.Debug.WriteLine($"Turning off stalled motor {MotorMonitored.ConnectedPort}.");
                    MotorMonitored.Off();
                    turnedOffStalledMotor = true;
            }
        }
    }
}
