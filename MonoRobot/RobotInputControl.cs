﻿using System;
using System.Collections.Generic;
using MonoBrick.EV3;
using MathNet.Spatial.Euclidean;
using static MonoRobot.KeyboardNumpadKeymap;

namespace MonoRobot
{
    //  Robot Input Control
    //  partial implementation of Robot class
    //  specifically to handle joystick and keyboard input control
    partial class Robot
    {

        /// <summary>
        /// Get Motor
        /// </summary>
        private Motor GetMotor(int i)
        {
            if (i < Motors.Count)
            {
                return Motors[i];
            }
            return null;
        }
        

        /// <summary>
        /// Process set motor command
        /// creates the command function to set motor and puts it in the processing queue
        /// </summary>
        void ProcessSetMotor(int motorIndex, int speed)
        {
            Motor motor = GetMotor(motorIndex);
            if (motor == null)
                return;

            RobotCommand cmd = new RobotCommand(motor, () =>
            {
                motor.On((sbyte)speed, false);
            });

            QueueCommand(cmd);
        }
        

        /// <summary>
        /// Process turn motor command
        /// creates the command function to turn motor and puts it in the processing queue
        /// </summary>
        void ProcessTurnMotor(int motorIndex, int degrees, int speed)
        {
            Motor motor = GetMotor(motorIndex);
            if (motor == null)
                return;

            RobotCommand cmd = new RobotCommand(motor, () =>
            {
                //motor.MoveTo((byte)speed, degrees, true);
                motor.On((sbyte)speed, (uint)degrees, true, false); 
            });

            QueueCommand(cmd);
        }
        

        /// <summary>
        /// Process motor off
        /// creates the command function to turn motor off and puts it in the processing queue
        /// </summary>
        void ProcessMotorOff(int motorIndex, bool resetTach = false)
        {
            Motor motor = GetMotor(motorIndex);
            if (motor == null)
                return;
            RobotCommand cmd = new RobotCommand(motor, () =>
            {
                motor.Off(false);
               
                if (resetTach)
                    motor.ResetTacho();
            });

            QueueCommand(cmd);
        }



        //  Driving Inputs 
        //

        //  Joystick Control State
        //
        JoystickDirection LastJoystickDirection = JoystickDirection.Stop;
        Int32 LastJoystickSpeed = 0;
        Int32 LastArmSpeed = 0;
        Int32 LastClawSpeed = 0;
        Int32 LastJoystickTurnPercentage = 0;

        Int32 TrackDrivingSpeedLeft = 0;
        Int32 TrackDrivingSpeedRight = 0;

        //  Properties
        Motor RobotLeftDriveMotor = null;
        Motor RobotRightDriveMotor = null;




        #region Joystick

        protected Vector2D VectorReferenceN = new Vector2D(0, 1);
        protected Vector2D VectorReferenceS = new Vector2D(0, -1);

        //  constant for one fifth of a quadrant
        static double FIFTHQUAD = ((Math.PI / 2.0) / 5.0);

        //  prevent mashing of buttons
        int ABtnChangeTick = 0;
        int BBtnChangeTick = 0;
        int XBtnChangeTick = 0;
        int YBtnChangeTick = 0;
        int ButtonDelay = 333;

        bool ABtnReady()
        {
            var tickNow = System.Environment.TickCount;
            if (System.Environment.TickCount - ABtnChangeTick > ButtonDelay)
            {
                ABtnChangeTick = tickNow;
                return true;
            }
            return false;
        }

        bool BBtnReady()
        {
            var tickNow = System.Environment.TickCount;
            if (System.Environment.TickCount - BBtnChangeTick > ButtonDelay)
            {
                BBtnChangeTick = tickNow;
                return true;
            }
            return false;
        }

        bool XBtnReady()
        {
            var tickNow = System.Environment.TickCount;
            if (System.Environment.TickCount - XBtnChangeTick > ButtonDelay)
            {
                XBtnChangeTick = tickNow;
                return true;
            }
            return false;
        }

        bool YBtnReady()
        {
            var tickNow = System.Environment.TickCount;
            if (System.Environment.TickCount - YBtnChangeTick > ButtonDelay)
            {
                YBtnChangeTick = tickNow;
                return true;
            }
            return false;
        }


       


        /// <summary>
        /// Process XBox Controller Input - Right Stick Mode
        /// </summary>
        protected void ProcessXboxControllerInputRightStickMode(SimpleJoy.XBoxJoystickEventArgs data)
        {
            if (Brick == null || !IsConnected)
                return;

            //  Right Button is driving motors kill switch
            if (data.Data.RightBtn)
            {
                KillSwitchVehicle();
            }
            else
            {
                ProcessXBoxControllerRightStickVehicleControl(data);
            }

            //  Handle inputs for arm motors
            if (data.Data.LeftBtn)
            {
                KillSwitchArmAndClaw();
            }
            else
            {
                ProcessXBoxControllerButtonsClawPinchControl(data);
                ProcessXBoxControllerButtonsArmControl(data);
                ProcessXboxControllerJoystickArmAndClawControl(data);
            }

        }


        /// <summary>
        /// Process XBox button input for claw in pinch mode
        /// </summary>
        private void ProcessXBoxControllerButtonsClawPinchControl(SimpleJoy.XBoxJoystickEventArgs data)
        {
            // Motor A - claw
            if (data.Data.XBtn && XBtnReady())
            {
                OpenClaw();
                return;
            }
            else if (data.Data.ABtn && ABtnReady())
            {
                CloseClaw();
                return;
            }

           
        }


        private void ProcessXBoxControllerButtonsClawAdjustControl(SimpleJoy.XBoxJoystickEventArgs data)
        {
            // Motor A - claw
            if (data.Data.XBtn && XBtnReady())
            {
                AdjustClawOpen();
                return;
            }
            else if (data.Data.ABtn && ABtnReady())
            {
                AdjustClawClose();
                return;
            }
        }

        /// <summary>
        /// Process XBox button input for arm up/down
        /// </summary>
        /// <param name="data"></param>
        private void ProcessXBoxControllerButtonsArmControl(SimpleJoy.XBoxJoystickEventArgs data)
        {
            //  Motor D - arm
            if (data.Data.YBtn && YBtnReady())
            {
                RaiseArm();
                return;
            }
            else if (data.Data.BBtn && BBtnReady())
            {
                LowerArm();
                return;
            }
        }


        /// <summary>
        /// Process XBox joystick input for arm and claw control
        /// </summary>
        private void ProcessXboxControllerJoystickArmAndClawControl(SimpleJoy.XBoxJoystickEventArgs data)
        {
            //  joystick for arm and claw
            //  apply the throttle multiplier from the right trigger
            double speedArmMultiplier = data.Data.LeftTrigger * 100.0;

            int speedArm = (int)(Math.Abs(data.Data.LeftStick.Y) > .5 ? Math.Max(30, speedArmMultiplier) : 0);
            if (data.Data.LeftStick.Y > 0)
                speedArm *= -1;
            if (speedArm == 0 && LastArmSpeed != 0)
            {
                //  turn arm off
                RobotCommand cmd = new RobotCommand(Brick.MotorD, () =>
                {
                    Brick.MotorD.Off();
                    MotorStateChanged?.Invoke(this, new MotorEventArgs(speedArm != 0, speedArm, Brick.MotorD.TachoCount));
                });
                QueueCommand(cmd);

                LastArmSpeed = 0;
            }
            else if (Math.Abs(speedArm - LastArmSpeed) >= 5)
            {
                RobotCommand cmd = new RobotCommand(Brick.MotorD, () =>
                {
                    Brick.MotorD.On((sbyte)(speedArm));
                    MotorStateChanged?.Invoke(this, new MotorEventArgs(speedArm != 0, speedArm, Brick.MotorD.TachoCount));
                });
                QueueCommand(cmd);
                LastArmSpeed = speedArm;
            }

            double speedClawMultiplier = data.Data.LeftTrigger * 10;
            int speedClaw = (int)(Math.Abs(data.Data.LeftStick.X) > .5 ? Math.Max(4, speedClawMultiplier) : 0);
            if (data.Data.LeftStick.X < 0)
                speedClaw *= -1;
            if (speedClaw == 0 && LastClawSpeed != 0)
            {
                //  turn claw off
                RobotCommand cmd = new RobotCommand(Brick.MotorA, () =>
                {
                    Brick.MotorA.Off();
                    MotorStateChanged?.Invoke(this, new MotorEventArgs(speedClaw != 0, speedClaw, Brick.MotorA.TachoCount));
                });
                QueueCommand(cmd);
                LastClawSpeed = 0;
            }
            else if (Math.Abs(speedClaw - LastClawSpeed) >= 2)
            {
                RobotCommand cmd = new RobotCommand(Brick.MotorA, () =>
                {
                    Brick.MotorA.On((sbyte)(speedClaw));
                    MotorStateChanged?.Invoke(this, new MotorEventArgs(speedClaw != 0, speedClaw, Brick.MotorA.TachoCount));
                });
                QueueCommand(cmd);
                LastClawSpeed = speedClaw;
            }
        }


        /// <summary>
        /// Process XBox joystick input for right stick vehicle control
        /// </summary>
        private void ProcessXBoxControllerRightStickVehicleControl(SimpleJoy.XBoxJoystickEventArgs data)
        {
            double speedRaw = data.Data.RightStick.Length > .15 ? data.Data.RightStick.Length * 30.0 : 0.0;

            //  apply the throttle multiplier from the right trigger
            double speedThrottle = data.Data.RightTrigger * 70.0;
            if (speedRaw > 0.0)
                speedRaw += speedThrottle;

            //  round this the the nearest five as an integer
            //  make sure it is no larger than 100
            Int32 speed = Math.Min(100, (Int32)Math.Round(speedRaw / 5.0) * 5);

            //  process the driving input
            ProcessDrivingInput(speed, data.Data.RightStick);
        }


        /// <summary>
        /// Process XBox Controller Joystick - Track Steering Mode
        /// </summary>
        void ProcessXboxControllerInputTrackSteeringMode(SimpleJoy.XBoxJoystickEventArgs data)
        {
            if (Brick == null || !IsConnected)
                return;

            //  driving inputs
            if (data.Data.RightBtn)
            {
                KillSwitchVehicle();
            }
            else
            {
                ProcessXBoxControllerTrackSteeringVehicleControl(data);
            }


            //  arm inputs
            if (data.Data.LeftBtn)
            {
                KillSwitchArmAndClaw();
            }
            else
            {
                ProcessXBoxControllerButtonsClawAdjustControl(data);

                ProcessXBoxControllerButtonsArmControl(data);
            }
        }

        /// <summary>
        /// Process XBox joystick inputs for track steering vehicle control
        /// </summary>
        private void ProcessXBoxControllerTrackSteeringVehicleControl(SimpleJoy.XBoxJoystickEventArgs data)
        {


            //  Set the speed between with max of 30
            double speedLeftRaw = Math.Abs(data.Data.LeftStick.Y) > .15 ? data.Data.LeftStick.Y * 30.0 : 0.0;
            double speedRightRaw = Math.Abs(data.Data.RightStick.Y) > .15 ? data.Data.RightStick.Y * 30 : 0.0;

            //  apply the throttle multiplier from the right trigger
            double speedThrottle = data.Data.RightTrigger * 70.0;

            //  set the final speed
            if (speedLeftRaw > 0.0)
                speedLeftRaw += speedThrottle;
            else if (speedLeftRaw < 0.0)
                speedLeftRaw -= speedThrottle;

            if (speedRightRaw > 0.0)
                speedRightRaw += speedThrottle;
            else if (speedRightRaw < 0.0)
                speedRightRaw -= speedThrottle;

            //  round this the the nearest five as an integer
            Int32 speedLeft = Math.Min(100, (Int32)Math.Round(speedLeftRaw / 5.0) * 5);
            Int32 speedRight = Math.Min(100, (Int32)Math.Round(speedRightRaw / 5.0) * 5);



            //  send command to motor.
            if (speedLeft == 0 && speedRight == 0)
            {
                if (TrackDrivingSpeedRight != 0 || TrackDrivingSpeedLeft != 0)
                {
                    RobotCommand cmd = new RobotCommand(Brick.Vehicle, () =>
                    {
                        Brick.Vehicle.Off();
                        TrackDrivingSpeedLeft = 0;
                        TrackDrivingSpeedRight = 0;
                    });
                    QueueCommand(cmd);

                }
            }
            else if ((speedLeft != TrackDrivingSpeedLeft) || (speedRight != TrackDrivingSpeedRight))
            {
                RobotCommand cmd = new RobotCommand(Brick.Vehicle, () =>
                {
                    //  left track speed
                    if (Math.Abs(speedLeft - TrackDrivingSpeedLeft) > 10)
                    {
                        RobotLeftDriveMotor.On((sbyte)speedLeft);
                        TrackDrivingSpeedLeft = speedLeft;
                        MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                    }

                    if (Math.Abs(speedRight - TrackDrivingSpeedRight) > 10)
                    {
                        RobotRightDriveMotor.On((sbyte)speedRight);
                        TrackDrivingSpeedRight = speedRight;
                        MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                    }

                });
                QueueCommand(cmd);
            }
        }


        /// <summary>
        /// Stop both driving motors
        /// </summary>
        private void KillSwitchVehicle()
        {
            QueueCommand(new RobotCommand(Brick.Vehicle, () =>
            {
                Brick.Vehicle.Off();
            }));

            LastJoystickSpeed = 0;
            LastJoystickTurnPercentage = 0;
            LastJoystickDirection = JoystickDirection.Stop;
        }


        /// <summary>
        /// Stop both the arm and claw motors
        /// </summary>
        private void KillSwitchArmAndClaw()
        {
            //  turn arm off
            RobotCommand cmd = new RobotCommand(Brick.MotorD, () =>
            {
                Brick.MotorD.Off();
                MotorStateChanged?.Invoke(this, new MotorEventArgs(false, 0, 0));
            });
            QueueCommand(cmd);
            LastArmSpeed = 0;

            //  turn claw off
            cmd = new RobotCommand(Brick.MotorA, () =>
            {
                Brick.MotorA.Off();
                MotorStateChanged?.Invoke(this, new MotorEventArgs(false, 0, 0));
            });
            QueueCommand(cmd);
        }


        /// <summary>
        /// Joystick Driving Direction
        /// Take vector for joystick, and determine direction and turning percentage
        /// </summary>
        private void JoystickDrivingDirection(Vector2D joystick, out JoystickDirection direction, out int turningPercentage)
        {
            turningPercentage = 0;
            direction = JoystickDirection.Forward;

            //  determine the quadrant
            quadrant quad = quadrant.NE;
            //
            if (joystick.X >= 0 && joystick.Y >= 0)
                quad = quadrant.NE;
            else if (joystick.X < 0 && joystick.Y >= 0)
                quad = quadrant.NW;
            else if (joystick.X < 0 && joystick.Y < 0)
                quad = quadrant.SW;
            else
                quad = quadrant.SE;

            //  determine the angle
            double angle = 0.0;
            switch (quad)
            {
                case quadrant.NE:
                    case quadrant.NW:
                    angle = joystick.AngleTo(VectorReferenceN).Radians;
                    break;

                    case quadrant.SE:
                    case quadrant.SW:
                    angle = joystick.AngleTo(VectorReferenceS).Radians;
                    break;
            }

            //  Check the angle, range is first fifth = forward, next three fifths are turn, final fifth is spin
            if (angle < FIFTHQUAD)
            {
                //  straight ahead or behind in direction
                switch (quad)
                {
                    case quadrant.NE:
                        case quadrant.NW:
                        direction = JoystickDirection.Forward;
                        break;
                        case quadrant.SE:
                        case quadrant.SW:
                        direction = JoystickDirection.Reverse;
                        break;
                }
            }
            else if (angle >= FIFTHQUAD && angle < (4.0 * FIFTHQUAD))
            {
                //  turning, determine the turning percent
                if (angle >= FIFTHQUAD && angle < 2.0 * FIFTHQUAD)
                    turningPercentage = 20;
                else if (angle >= 2.0 * FIFTHQUAD  && angle < 3.0 * FIFTHQUAD)
                    turningPercentage = 60;
                else if (angle >= 3.0 * FIFTHQUAD)
                    turningPercentage = 80;

                //  forward turn or behind turn
                switch (quad)
                {
                    case quadrant.NE:
                        direction = JoystickDirection.ForwardRight;
                        break;
                        case quadrant.SE:
                        direction = JoystickDirection.ReverseRight;
                        break;
                        case quadrant.NW:
                        direction = JoystickDirection.ForwardLeft;
                        break;
                        case quadrant.SW:
                        direction = JoystickDirection.ReverseLeft;
                        break;
                }
            }
            else
            {
                //  forward turn or behind turn
                switch (quad)
                {
                    case quadrant.NE:
                        case quadrant.SE:
                        direction = JoystickDirection.SpinRight;

                        break;
                        case quadrant.NW:
                        case quadrant.SW:
                        direction = JoystickDirection.SpinLeft;
                        break;
                }
            }
        }


        /// <summary>
        /// Process driving input
        /// </summary>
        protected void ProcessDrivingInput(int speed, Vector2D joystick)
        {
            //  if we are not moving fast enough to matter, just bail
            if (speed < 10)
            {
                if (LastJoystickSpeed == 0)
                    return;
                else
                {
                    QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                    {
                        Brick.Vehicle.Off();
                    }));

                    LastJoystickSpeed = 0;
                    LastJoystickTurnPercentage = 0;
                    LastJoystickDirection = JoystickDirection.Stop;
                }
            }
            else
            {
                int thisTurningPercentage = 0;
                JoystickDirection thisDirection = JoystickDirection.Stop;

                JoystickDrivingDirection(joystick, out thisDirection, out thisTurningPercentage);

                //  Did we make a change worth applying
                if (thisDirection != LastJoystickDirection ||
                    Math.Abs(speed - LastJoystickSpeed) > 10 ||
                    (IsTurning(thisDirection) && thisTurningPercentage != LastJoystickTurnPercentage))
                {
                   
                    if (Brick != null && IsConnected)
                    {
                        RobotCommand cmd = null;
                        //  set this item
                        switch (thisDirection)
                        {
                            case JoystickDirection.Forward:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.Forward((sbyte)speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;
                            case JoystickDirection.ForwardRight:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.TurnRightForward((sbyte)speed, (sbyte)thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;
                            case JoystickDirection.SpinRight:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.SpinRight((sbyte)speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.ReverseRight:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.TurnRightReverse((sbyte)speed, (sbyte)thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.Reverse:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.Backward((sbyte)speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.ReverseLeft:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.TurnLeftReverse((sbyte)speed, (sbyte)thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.SpinLeft:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.SpinLeft((sbyte)speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.ForwardLeft:
                                cmd = new RobotCommand(Brick.Vehicle, () =>
                                {
                                    Brick.Vehicle.TurnLeftForward((sbyte)speed, (sbyte)thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;
                        }


                        if (cmd != null)
                            QueueCommand(cmd);
                    }
                    LastJoystickSpeed = speed;
                    LastJoystickDirection = thisDirection;
                    LastJoystickTurnPercentage = IsTurning(LastJoystickDirection) ? thisTurningPercentage : 0;
                }           
            }
        }


        /// <summary>
        /// Is turning
        /// helper function to translate joystick direction into bool to indicate turning or straight motion
        /// </summary>
        private bool IsTurning(JoystickDirection dir)
        {
            switch (dir)
            {
                case JoystickDirection.ForwardRight:
                case JoystickDirection.ForwardLeft:
                case JoystickDirection.ReverseRight:
                case JoystickDirection.ReverseLeft:
                    return true;

                default:
                    return false;
            }
        }

        #endregion


        #region KeyboardInput

        bool ForwardDirection = false;
        sbyte DrivingSpeed = 10;
        ConsoleKey lastKey = ConsoleKey.Spacebar;

        /// <summary>
        /// Process Keyboard Driving Input
        /// </summary>
        /// <param name="key">key pressed</param>
        void ProcessKeyboardDrivingInput(int key)
        {
            ConsoleKey keyValue = (ConsoleKey)key;
            Logging.LogFunction(this, "ProcessKeyboardDrivingInput", LogLevel.DEBUG, $"Input key {keyValue}.");

            switch (keyValue)
            {
                //  Reset arm and claw tacho
                case ConsoleKey.Divide:
                case ConsoleKey.Oem2:
                    ArmMotorsResetTachoCount();
                    break;

                //  Arm MotorA/D - All Off
                case ConsoleKey.H:
                    ArmMotorsAllOff();
                    break;

                //  Open Claw - Motor A
                case ConsoleKey.Z:
                case ConsoleKey.NumPad1:
                case ConsoleKey.D1:
                    OpenClaw();
                    break;

                case ConsoleKey.NumPad3:
                case ConsoleKey.D3:
                case ConsoleKey.C:
                    CloseClaw();
                    break;

                //  Raise Arm - Motor D
                case ConsoleKey.NumPad0:
                case ConsoleKey.D0:
                case ConsoleKey.V:
                    RaiseArm();
                    break;

                //  Lower Arm - Motor D
                case ConsoleKey.Decimal:
                case ConsoleKey.B:
                    LowerArm();
                    break;

                //  Claw - Motor A increase speed clockwize (facing robot)
                case ConsoleKey.R:
                    AdjustClawMotorASpeed(5);
                    break;

                //  Claw - Motor A increase speed counter clockwize (facing robot)
                case ConsoleKey.F:
                    AdjustClawMotorASpeed(-5);
                    break;

                //  Arm - Motor D increase speed up
                case ConsoleKey.T:
                    AdjustArmMotorDSpeed(-5);
                    break;

                //  Arm - Motor D increase speed down
                case ConsoleKey.G:
                    AdjustArmMotorDSpeed(5);
                    break;

                //  Driving - forward direction
                case ConsoleKey.D8:
                case ConsoleKey.W:
                case ConsoleKey.NumPad8:
                    DrivingAdjustSpeedForward();
                    break;

                //  Driving - reverse direction
                case ConsoleKey.D2:
                case ConsoleKey.X:
                case ConsoleKey.NumPad2:
                    DrivingAdjustSpeedReverse();
                    break;

                //  Driving left direction
                case ConsoleKey.D4:
                case ConsoleKey.A:
                case ConsoleKey.NumPad4:
                    DrivingAdjustSpeedLeft();
                    break;

                //  Driving - right direction
                case ConsoleKey.D6:
                case ConsoleKey.D:
                case ConsoleKey.NumPad6:
                    DrivingAdjustSpeedRight();
                    break;

                //  Driving - spin left
                case ConsoleKey.D7:
                case ConsoleKey.Q:
                case ConsoleKey.NumPad7:
                    DrivingSpinAdjustLeft();
                    break;

                //  Driving spin right
                case ConsoleKey.D9:
                case ConsoleKey.E:
                case ConsoleKey.NumPad9:
                    DrivingSpinAdjustRight();
                    break;


                //  All motors off
                case ConsoleKey.D5:
                case ConsoleKey.S:
                case ConsoleKey.NumPad5:
                    AllMotorsStop();
                    break;
            }

            lastKey = (ConsoleKey)key;
            return;

        }

        #endregion


        #region ArmMotorControl

        private void LowerArm()
        {
            QueueCommand( new RobotCommand(Brick.MotorD, () =>
            {
                MotorSetTurn(3, 360, 25);
            }));
        }

        private void RaiseArm()
        {
            QueueCommand (new RobotCommand(Brick.MotorD, () =>
            {
                MotorSetTurn(3, 360, -25);
            }));
        }

        private void AdjustArmMotorDSpeed(int change)
        {
            RobotCommand cmd = new RobotCommand(Brick.MotorD, () =>
            {
                int speed = Brick.MotorD.Speed + change;
                if (speed < 5 && speed > -5)
                {
                    speed = 0;
                    Brick.MotorD.Off();
                }
                else
                {
                    Brick.MotorD.On((sbyte)(speed));
                }
            });
            QueueCommand(cmd);
        }

        private void OpenClaw()
        {
            var tach = Brick.MotorA.TachoCount;
            if ( tach > 0 )
            {
                QueueCommand( new RobotCommand(Brick.MotorA, () =>
                {
                    MotorSetTurn(0, tach, -30);
                }));
            }
            else
            {
                QueueCommand( new RobotCommand(Brick.MotorA, () =>
                {
                    MotorSetTurn(0, Math.Abs(tach), 30);
                }));
            }
        }

        private void CloseClaw()
        {
            var tach = Brick.MotorA.TachoCount;
            if (tach > 133)
                return;

            QueueCommand(new RobotCommand(Brick.MotorA, () =>
            {
                MotorSetTurn(0, 140 - Brick.MotorA.TachoCount, 30);
            }));

        }

        private void AdjustClawOpen()
        {
            var tach = Brick.MotorA.TachoCount;
            if (tach < -133 )
                return;

            QueueCommand(new RobotCommand(Brick.MotorA, () =>
            {
                MotorSetTurn(0, 20, -20);
            }));
        }

        private void AdjustClawClose()
        {
            var tach = Brick.MotorA.TachoCount;
            if (tach > 133 )
                return;

            QueueCommand(new RobotCommand(Brick.MotorA, () =>
            {
                MotorSetTurn(0, 20, 20);
            }));
        }

        void AdjustClawMotorASpeed(int change)
        {
            RobotCommand cmd = new RobotCommand(Brick.MotorA, () =>
            {
                int speed = Brick.MotorA.Speed + change;
                if (speed < 5 && speed > -5)
                {
                    speed = 0;
                    Brick.MotorA.Off();
                }
                else
                {
                    Brick.MotorA.On((sbyte)(speed));
                }
            });
            QueueCommand(cmd);
        }

        private void ArmMotorsAllOff()
        {
            //  this is zero, turn the motor off
            QueueCommand(new RobotCommand(Brick.MotorA, () =>
            {
                Brick.MotorA.Off();
            }));
            QueueCommand(new RobotCommand(Brick.MotorD, () =>
            {
                Brick.MotorD.Off();
            }));
        }

        private void ArmMotorsResetTachoCount()
        {
            QueueCommand(new RobotCommand(Brick.MotorA, () =>
            {
                Brick.MotorA.ResetTacho();
            }));
            QueueCommand(new RobotCommand(Brick.MotorD, () =>
            {
                Brick.MotorD.ResetTacho();
            }));
        }
       
        #endregion


        #region VehicleControl

        private void DrivingSpinAdjustRight()
        {
            if (IsD9Key(lastKey) || DrivingSpeed == 0)
                DrivingSpeed += 10;
            DrivingSpeed = Math.Min(DrivingSpeed, (sbyte)100);

            QueueCommand(new RobotCommand(Brick.Vehicle, () =>
            {
                Brick.Vehicle.SpinRight(DrivingSpeed);
            }));
        }

        private void DrivingSpinAdjustLeft()
        {
            if (IsD7Key(lastKey) || DrivingSpeed == 0)
                DrivingSpeed += 10;
            DrivingSpeed = Math.Min(DrivingSpeed, (sbyte)100);

            QueueCommand(new RobotCommand(Brick.Vehicle, () =>
            {
                Brick.Vehicle.SpinLeft(DrivingSpeed);
            }));
        }

        private void DrivingAdjustSpeedRight()
        {
            if (IsD6Key(lastKey) || DrivingSpeed == 0)
                DrivingSpeed += 10;
            DrivingSpeed = Math.Min(DrivingSpeed, (sbyte)100);

            if (ForwardDirection)
            {
                QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                {
                    Brick.Vehicle.TurnRightForward(DrivingSpeed, 50);
                }));
            }
            else
            {

                QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                {
                    Brick.Vehicle.TurnLeftReverse(DrivingSpeed, 50);
                }));

                ForwardDirection = false;
            }
        }

        private void DrivingAdjustSpeedLeft()
        {
            if (IsD4Key(lastKey) || DrivingSpeed == 0)
                DrivingSpeed += 10;
            DrivingSpeed = Math.Min(DrivingSpeed, (sbyte)100);

            if (ForwardDirection)
            {
                QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                {
                    Brick.Vehicle.TurnLeftForward(DrivingSpeed, 50);
                }));
            }
            else
            {
                QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                {
                    Brick.Vehicle.TurnRightReverse(DrivingSpeed, 50);
                }));

                ForwardDirection = false;
            }
        }

        private void DrivingAdjustSpeedReverse()
        {
            //  speed up or slow down in the current direction
            if (ForwardDirection && DrivingSpeed != 0)
            {
                DrivingSpeed -= 10;
                //  did we stop
                if (DrivingSpeed == 0)
                {
                    QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                    {
                        Brick.Vehicle.Off();
                    }));

                    ForwardDirection = true;
                    DrivingSpeed = 0;
                }
                else
                {
                    QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                    {
                        Brick.Vehicle.Forward(DrivingSpeed);
                    }));
                }
            }
            else
            {
                ForwardDirection = false;
                if (IsD2Key(lastKey) || DrivingSpeed == 0)
                    DrivingSpeed += 10;
                DrivingSpeed = Math.Min(DrivingSpeed, (sbyte)100);

                QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                {
                    Brick.Vehicle.Backward(DrivingSpeed);
                }));
            }
        }

        private void DrivingAdjustSpeedForward()
        {
            //  speed up or slow down in the current direction
            if (!ForwardDirection)
            {
                DrivingSpeed -= 10;
                //  did we stop
                if (DrivingSpeed == 0)
                {
                    QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                    {
                        Brick.Vehicle.Off();
                    }));

                    ForwardDirection = true;
                    DrivingSpeed = 0;
                }
                else
                {
                    QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                    {
                        Brick.Vehicle.Backward(DrivingSpeed);
                    }));
                }
            }
            else
            {
                ForwardDirection = true;

                if (IsD8Key(lastKey) || DrivingSpeed == 0)
                    DrivingSpeed += 10;
                DrivingSpeed = Math.Min(DrivingSpeed, (sbyte)100);

                QueueCommand(new RobotCommand(Brick.Vehicle, () =>
                {
                    Brick.Vehicle.Forward(DrivingSpeed);
                }));
            }
        }

        private void AllMotorsStop()
        {
            QueueCommand(new RobotCommand(Brick.Vehicle, () =>
            {
                Brick.Vehicle.Off();
            }));

            //  driving parameters init
            ForwardDirection = true;
            DrivingSpeed = 0;

            //  this is zero, turn the motor off
            QueueCommand(new RobotCommand(Brick.MotorA, () =>
            {
                Brick.MotorA.Off();
            }));

            //  this is zero, turn the motor off
            QueueCommand(new RobotCommand(Brick.MotorD, () =>
            {
                Brick.MotorD.Off();
            }));
        }

        #endregion




        /// <summary>
        /// Process the command queue
        /// do this pre processing to remove redundant commands
        /// only one command from each object is allowed
        /// and motor control commands will cancel status update commands
        /// </summary>
        List<RobotCommand> ProcessCommandQueue(List<RobotCommand> queue)
        {
            List<RobotCommand> filteredQueue = new List<RobotCommand>();

            //  one vehicle command allowed
            Object vehicleCommand = null;
            //  one brick (status) command allowed
            Object brickCommand = null;

            foreach (var nextCommand in queue)
            {
                if (nextCommand.CommandObject == Brick.Vehicle)
                {
                    if (vehicleCommand == null)
                    {
                        vehicleCommand = Brick.Vehicle;
                        filteredQueue.Add(nextCommand);
                    }
                    else
                        continue;
                }
                else if (nextCommand.CommandObject.GetType() == typeof(Motor))
                {
                    //  process motors
                    //  reject this command if superceeded by another motor command or vehicle command
                    if (vehicleCommand != null && (nextCommand.CommandObject == RobotLeftDriveMotor || nextCommand.CommandObject == RobotRightDriveMotor))
                        continue;
                    else if (filteredQueue.Find(x => x.CommandObject == nextCommand.CommandObject) != null)
                        continue;
                    else
                    {
                        //  todo - find this motor and filter
                        filteredQueue.Add(nextCommand);
                    }
                }
                else if (nextCommand.CommandObject.GetType() == typeof(Sensor))
                {
                    //  process sensor command  
                    filteredQueue.Add(nextCommand);
                }
                else if (nextCommand.CommandObject.GetType() == typeof(Brick<Sensor, Sensor, Sensor, Sensor>))
                {
                    if (brickCommand == null)
                        brickCommand = nextCommand.CommandObject;
                    else
                        continue;

                    // process brick command 
                    filteredQueue.Add(nextCommand);
                }
                else
                    filteredQueue.Add(nextCommand);
            }

            //  now dump status queries if we have pending motor commands
            if (vehicleCommand != null || filteredQueue.Find(x => x.CommandObject.GetType() == typeof(Motor)) != null)
                filteredQueue.RemoveAll(x => x.CommandObject.GetType() == typeof(Brick<Sensor, Sensor, Sensor, Sensor>));

            return filteredQueue;
        }

    }
}

