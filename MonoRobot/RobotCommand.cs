﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    /// <summary>
    /// Container class to hold a function to send to the robot
    /// </summary>
    class RobotCommand
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="commandObject">object that is issuing this command</param>
        /// <param name="command">delegate function to execute when this command runs</param>
        public RobotCommand(Object commandObject, Action command)
        {
            CommandObject = commandObject;
            Command = command;
        }

        public Object CommandObject;
        public Action Command;
    }
}
